package fr.mathieubourgault.photoparty;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements PhotopartyServiceReceiver{

    private EditText editText;
    private TextView textView;

    public static final String LOGIN="login";
    private String login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.editText = (EditText) findViewById(R.id.loginInput);
        this.textView = (TextView) findViewById(R.id.textView);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        login = preferences.getString(LOGIN, "");
        this.editText.setText(login);

         PhotopartyWifi.connect(getApplicationContext());

        //PhotopartyService.getInstance().getConfig(this);
    }

    public void onLogin(View v){
       if (this.editText.getText().length()>0){
            PhotopartyService service = PhotopartyService.getInstance();
            service.register(this,this.editText.getText().toString());
        }else{
            logError("Vous devez entrer un identifiant");
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        save();
    }

    private void save() {
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putString(LOGIN, login);
        editor.commit();
    }

    public void startUploadActivity(){
        Intent i = new Intent(this, UploadActivity.class);
        i.putExtra(LoginActivity.LOGIN,this.editText.getText().toString());
        startActivity(i);
        this.finish();
    }

    public void logError(String message){
        this.textView.setVisibility(View.VISIBLE);
        this.textView.setText(Html.fromHtml(message).toString());
    }

    @Override
    public void logSuccess(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        startUploadActivity();
    }

    @Override
    public void logJSON(JSONObject jsonObject) {
        if(jsonObject.has("nom")){
            try {
                this.login = jsonObject.getString("nom");
                this.editText.setText(login);
                startUploadActivity();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
