package fr.mathieubourgault.photoparty;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONObject;


public class VoteActivity extends AppCompatActivity implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener,PhotopartyServiceReceiver{

    private static final String DEBUG_TAG = "Vote";
    private GestureDetectorCompat mDetector;
    private SensorManager mSensorManager = null;
    private Sensor mAccelerometer = null;
    private RadioGroup radioGroup;

    private String login;
    private int vote;

    private float scaleX,scaleY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);

        Intent intent = getIntent();
        login = intent.getStringExtra(LoginActivity.LOGIN);



        mDetector  = new GestureDetectorCompat(this,this);
        // Set the gesture detector as the double tap
        // listener.
        mDetector.setOnDoubleTapListener(this);
        radioGroup = (RadioGroup) findViewById(R.id.radiogroup);

        newSelected = findViewById(R.id.cool);
        scaleX = newSelected.getScaleX();
        scaleY = newSelected.getScaleY();
        progress(0);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if(mAccelerometer != null){
            mSensorManager.registerListener(mSensorEventListener,mAccelerometer,SensorManager.SENSOR_DELAY_GAME);
        }else{
            Toast.makeText(getApplicationContext(),"Accelerometrer not available",Toast.LENGTH_LONG).show();
        }

    }


    private int x=0,y=0,z=0;

    public boolean isInInterval(double d,int interval,int n ){
        return ((d <= (n-interval))||(d >= (n+interval)));
    }

    final SensorEventListener mSensorEventListener = new SensorEventListener() {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {

            if((isInInterval(sensorEvent.values[0],2,x))
                    &&(isInInterval(sensorEvent.values[1],2,y))&&
                    (isInInterval(sensorEvent.values[2],2,z))){

                x=Math.round(sensorEvent.values[0]);
                y=Math.round(sensorEvent.values[1]);
                z=Math.round(sensorEvent.values[2]);

                progress(1);
            }

        }
    };


    private void progress(int add){
        vote += add;
        getIntent().putExtra("vote",vote);
        if ( vote > 150){
            vote = 5;
        };

        ProgressBar pb = (ProgressBar)findViewById(R.id.progressBar);
        View v;
        if(vote > 100){
            pb.getProgressDrawable().setColorFilter(Color.parseColor("#2ff203"), PorterDuff.Mode.MULTIPLY);
            v = findViewById(R.id.cool);

        }else if (vote > 55){
            pb.getProgressDrawable().setColorFilter(Color.parseColor("#ffec6d"), PorterDuff.Mode.MULTIPLY);
            v = findViewById(R.id.sympa);

        }else if (vote > 30){
            pb.getProgressDrawable().setColorFilter(Color.parseColor("#faab01"), PorterDuff.Mode.MULTIPLY);
            v = findViewById(R.id.moyen);

        }else {
            pb.getProgressDrawable().setColorFilter(Color.parseColor("#f10912"), PorterDuff.Mode.MULTIPLY);
            v = findViewById(R.id.bof);
        }
        if (v.getId() != newSelected.getId()){
            v.setBackgroundColor(Color.parseColor("#07b6b6b6"));
            //v.setPadding(1, 1, 1, 1);
          //  v.refreshDrawableState();
            newSelected.setBackgroundColor(Color.TRANSPARENT);
            //newSelected.setPadding(30, 30, 30, 30);
            //newSelected.refreshDrawableState();
            animateDeselected(newSelected);
            newSelected = v;
            animateSelected(newSelected);
        }
        pb.setProgress(vote);
    }


    private View newSelected;

    public void bof(View v){
        imageClick(15);
        animateSelected(v);
    }
    public void moyen(View v){
        imageClick(40);
        animateSelected(v);
    }
    public void sympa(View v){
        imageClick(60);
        animateSelected(v);
    }
    public void cool(View v){
        imageClick(101);
        animateSelected(v);
    }

    public void imageClick(int i){
        vote = i;
        progress(0);

    }



    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);

        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDown: " + event.toString());
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        //Toast.makeText(getApplicationContext(), "Fling Gesture", Toast.LENGTH_SHORT).show();
        if(velocityX > 100 ){
            startUploadActvity();
            //Toast.makeText(getApplicationContext(), "X !", Toast.LENGTH_SHORT).show();
        }
        Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        Log.d(DEBUG_TAG, "onScroll: " + e1.toString()+e2.toString());
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorEventListener, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        Bundle b = getIntent().getExtras();
        vote = (Integer)b.get("vote");
        progress(vote);
        save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mSensorEventListener, mAccelerometer);
        Bundle b = getIntent().getExtras();
        vote = (Integer)b.get("vote");
        progress(vote);
        save();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vote, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_login);
        item.setTitle(login);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.menu_upload) {
            startUploadActvity();

        }


        return super.onOptionsItemSelected(item);
    }



    private void save(){
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putString(LoginActivity.LOGIN,login);
        editor.commit();
    }

    public void voter(View view){
        int v = (int)Math.ceil((((double)this.vote)/101.0) * 4.0);
        char vote = 'n';
        switch(v){
            case 1:
                vote = 'n';
                break;
            case 2:
                vote = 's';
                break;
            case 3:
                vote = 'c';
                break;
            case 4:
                vote = 'g';
                break;
        }
        Log.d(DEBUG_TAG,"Valeur : "+(((double)this.vote)/101.0) * 4.0);
        Log.d(DEBUG_TAG, "AVIS : " + vote);
        PhotopartyService.getInstance().voter(this, vote);
    }

    @Override
    public void logError(String errorMessage) {
        Toast.makeText(this, Html.fromHtml(errorMessage), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void logSuccess(String message) {
        Toast.makeText(this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void logJSON(JSONObject jsonObject) {

    }

    public void animateSelected(View v){
        v.animate()
                .scaleXBy(0.3f)
                .scaleYBy(0.3f)
                .setDuration(500)
                .start();
    }
    public void animateDeselected(View v){
        v.animate()
                .scaleX(scaleX)
                .scaleY(scaleY)
                .setDuration(500)
                .start();
    }

    private void startUploadActvity(){
        save();
        Intent voteIntent = new Intent(this, UploadActivity.class);
        voteIntent.putExtra(LoginActivity.LOGIN,this.login);
        startActivity(voteIntent);
        this.finish();
    }
}