package fr.mathieubourgault.photoparty;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import java.util.List;

/**
 * Created by MathieuBourgault on 30/03/2016.
 */
public class PhotopartyWifi {



    public static void connect(Context context){

        String networkSSID = "PhotoParty";
        String networkPass = "jpoiut2016";

        WifiConfiguration wc = new WifiConfiguration();
        wc.SSID = String.format("\"%s\"", networkSSID);
        wc    .preSharedKey = String.format("\"%s\"", networkPass);

        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

        wc.status = WifiConfiguration.Status.ENABLED;
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        // connect to and enable the connection
        int netId = wifiManager.addNetwork(wc);
        wifiManager.enableNetwork(netId, true);
        wifiManager.setWifiEnabled(true);

        Toast.makeText(context, "Vous êtes connecté au wifi", Toast.LENGTH_SHORT).show();


    }
}
