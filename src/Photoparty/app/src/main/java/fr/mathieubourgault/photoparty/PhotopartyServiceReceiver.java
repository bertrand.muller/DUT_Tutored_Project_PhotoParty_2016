package fr.mathieubourgault.photoparty;

import org.json.JSONObject;

/**
 * Created by thomas on 20/03/16.
 */
public interface PhotopartyServiceReceiver {
    void logError(String errorMessage);
    void logSuccess(String message);
    void logJSON(JSONObject jsonObject);
}
