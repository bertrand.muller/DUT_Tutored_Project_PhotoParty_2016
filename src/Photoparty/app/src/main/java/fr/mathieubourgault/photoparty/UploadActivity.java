package fr.mathieubourgault.photoparty;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


public class UploadActivity extends AppCompatActivity implements
        GestureDetector.OnGestureListener,PhotopartyServiceReceiver{


    final int ACTIVITY_SELECT_IMAGE = 1234;
    final int ACTIVITY_TAKE_IMAGE = 4321;

    private static final String DEBUG_TAG = "UPLOAD";

    private GestureDetectorCompat mDetector;

    FrameLayout previewFrame;
    ImageView preview;

    private Bitmap image;
    private boolean isGallery;

    private String login;

    float dY, dYorigin;
    boolean send = false;

    private int screenWidth;
    private int screenHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        this.screenHeight = 600;
        this.screenWidth = 800;
        mDetector  = new GestureDetectorCompat(this,this);
        isGallery = false;
        this.image = null;


        preview = (ImageView) findViewById(R.id.imageView2);
        dYorigin = preview.getY();

        preview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTouchPreview(v, event);
            }
        });

        Intent intent = getIntent();
        login = intent.getStringExtra(LoginActivity.LOGIN);


        PhotopartyService.getInstance().getConfig(this);
    }

    /**
     * Détècte le mouvement
     * vers le haut de l'utilisateur pour
     * faire l'effet swipe
     * @param view
     * @param event
     * @return
     */
    public boolean onTouchPreview(View view, MotionEvent event) {

        if(send)return false;

        switch (event.getActionMasked()) {

            case MotionEvent.ACTION_DOWN:
                dY = view.getY() - event.getRawY();

                findViewById(R.id.info_relative).setVisibility(View.INVISIBLE);
                break;

            case MotionEvent.ACTION_MOVE:

                /*
                 * Vers le haut on envoie la photo
                 */
                if(view.getY() < -400){
                    if(image != null){
                        send = true;
                        uploader(view);
                    }
                    /*
                     * Vers le bas on charge la galerie
                     */

                }else if (view.getY() > 400){
                    if(!this.isGallery){
                        this.isGallery = true;
                        startDialog(view);
                    }


                    //Ouvrir la galerie
                }else{
                    view.animate()
                            .y(event.getRawY() + dY)
                            .setDuration(0)
                            .start();
                }

                break;

            default:

                view.animate()
                        .y(dYorigin)
                        .setDuration(1000)
                        .start();
                if(image==null)
                findViewById(R.id.info_relative).setVisibility(View.VISIBLE);
                isGallery = false;
                return false;
        }
        return true;
    }


    /**
     * affiche
     * @param v
     */
    public void onPreviewClick(View v) {
        Intent pictureActionIntent = null;
        pictureActionIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(pictureActionIntent,
                ACTIVITY_SELECT_IMAGE);
    }

    /**
     * Récupère les variables utile lors
     * du retour à l'activité
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ACTIVITY_SELECT_IMAGE:
                if (resultCode == RESULT_OK) {


                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();


                    Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath);
                    this.image = resizeBitmap(yourSelectedImage);
                    preview.setImageBitmap(this.image);
                }
                break;
            case ACTIVITY_TAKE_IMAGE:
                if (data!=null){
                    try {
                        Bundle extras = data.getExtras();
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        this.image = resizeBitmap(imageBitmap);
                        preview.setImageBitmap(this.image);
                    }catch (Exception e){}
                }

                break;
        }


        isGallery = false;

    }

    private Bitmap resizeBitmap(Bitmap bitmap){
        double scaleW = (double)this.screenWidth/bitmap.getWidth();
        double scaleH = (double)this.screenHeight/bitmap.getHeight();
        double scale = 1;
        if(scaleW < 1){
            if(scaleH <= scaleW){
                scale = scaleH;
            }
        }

        if(scaleH < 1){
            if(scaleW <= scaleH){
                scale = scaleW;
            }
        }

        int newWidth =(int)Math.floor(bitmap.getWidth() * scale);
        int newHeight =(int)Math.floor(bitmap.getHeight() * scale);
        /* Alignement 4 octets */
        if((newWidth * 3) %4 != 0)
        {
            int reste = (newWidth * 3) % 4;
            newWidth = newWidth - (4 - reste);
        }
        if(newHeight % 2 != 0)newHeight-=1;
        return Bitmap.createScaledBitmap(bitmap,newWidth,newHeight,false);
    }
    /**
     * Ouvre un dialogue pour
     * que l'utilisateur puisse
     * choisie entre une image dans sa galerie
     * ou prendre une photo
     * @param v
     */
    private void startDialog(View v) {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder((Activity)v.getContext());
        myAlertDialog.setTitle("Choix de l'image");
        myAlertDialog.setMessage("Comment voulez-vous choisir la photo?");

        myAlertDialog.setPositiveButton("Galerie",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent pictureActionIntent = null;

                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(
                                pictureActionIntent,
                                ACTIVITY_SELECT_IMAGE);


                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        //dispatchTakePictureIntent();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(intent, ACTIVITY_TAKE_IMAGE);
                        }

                    }
                });
        myAlertDialog.show();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);

        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDown: " + event.toString());
        return true;
    }


    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        if(velocityX < 50 ){
            startVoteActvity();
        }

        Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
        return true;
    }



    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        Log.d(DEBUG_TAG, "onScroll: " + e1.toString() + e2.toString());
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public void onShowPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_upload, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_login);
        item.setTitle(login);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.menu_vote) {
            startVoteActvity();
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        save();
    }

    @Override
    protected void onStop() {
        super.onStop();
        save();
    }

    /**
     * Sauvegarde le pseudonyme
     */
    private void save(){
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putString(LoginActivity.LOGIN, login);
        editor.commit();
    }

    private void startVoteActvity(){
        save();
        Intent voteIntent = new Intent(this, VoteActivity.class);
        voteIntent.putExtra(LoginActivity.LOGIN,this.login);
        startActivity(voteIntent);
        this.finish();
    }

    public void uploader(View v){
        if(this.image != null){
            findViewById(R.id.info_relative).setVisibility(View.INVISIBLE);
            preview.animate()
                    .y(-2000)
                    .setDuration(1000)
                    .start();
            PhotopartyService.getInstance().upload(this,this.image);
        }else{
            Toast.makeText(this,"Choisissez une image",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void logError(String errorMessage) {
        Toast.makeText(this, Html.fromHtml(errorMessage),Toast.LENGTH_SHORT).show();
        reinitialiserPreview();
    }

    @Override
    public void logSuccess(String message) {
        Toast.makeText(this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();
        reinitialiserPreview();
    }

    @Override
    public void logJSON(JSONObject jsonObject) {
        try {
            this.screenWidth = jsonObject.getInt("width");
            this.screenHeight = jsonObject.getInt("height");
            System.out.println("new screenWidth : "+screenWidth);
            System.out.println("new screenHeight : "+screenHeight);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void reinitialiserPreview(){
        image = null;
        send = false;
        preview.setImageBitmap(null);
        preview.animate()
                .y(dYorigin)
                .setDuration(1000)
                .start();
    }
}
