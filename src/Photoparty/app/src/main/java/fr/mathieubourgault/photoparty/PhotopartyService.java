package fr.mathieubourgault.photoparty;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.renderscript.ScriptGroup;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by thomas on 17/03/16.
 */
public class PhotopartyService {

    private static final String TAG="PhotopartyService";
    private String urlService = "http://photoparty.fr/";
    private String PHPSESSID;
    private static PhotopartyService service;

    public static PhotopartyService getInstance(){
        if(service == null)service = new PhotopartyService();
        return service;
    }

    public void register(LoginActivity activity,String nom){
        AsyncHttpRequest httpRequest = new AsyncHttpRequest(activity,AsyncHttpRequest.LOGIN);
        httpRequest.addPostValue("pseudo",nom);
        httpRequest.addPostValue("bouton","valid");
        httpRequest.execute();
    }

    public void voter(VoteActivity activity,char vote){
        AsyncHttpRequest httpRequest = new AsyncHttpRequest(activity,AsyncHttpRequest.VOTER);
        httpRequest.addPostValue("avis", Character.toString(vote));
        httpRequest.addPostValue("vote","valid");
        httpRequest.execute();
    }

    public void upload(UploadActivity activity,Bitmap image)
    {
        AsyncHttpRequest httpRequest = new AsyncHttpRequest(activity,AsyncHttpRequest.UPLOADER);
        httpRequest.setBitmap(image);
        httpRequest.execute();
    }

    public void getConfig(PhotopartyServiceReceiver receiver){
        AsyncHttpRequest httpRequest = new AsyncHttpRequest(receiver,AsyncHttpRequest.CONFIG);
        httpRequest.execute();
    }

    private class AsyncHttpRequest extends AsyncTask<String,Void,JSONObject>{

        public static final String LOGIN="connexion",VOTER="vote",UPLOADER="upload",CONFIG="config";

        private String mode;
        private List<Pair<String,String>> postContent;

        private Bitmap image;
        private PhotopartyServiceReceiver receiver;


        public AsyncHttpRequest(PhotopartyServiceReceiver receiver,String mode){
            this.receiver = receiver;
            this.mode = mode;
            this.postContent = new ArrayList<Pair<String,String>>();
        }

        public void setBitmap(Bitmap image){
            this.image = image;
        }

        public HttpURLConnection openConnection() throws IOException {
            URL url = new URL(urlService+mode);
            Log.d(TAG,"Post on url :"+urlService+mode);
            System.out.println("Post on url : "+urlService+mode);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            if(mode == CONFIG)conn.setRequestMethod("GET");
            else conn.setRequestMethod("POST");
            System.out.println("Mode : "+conn.getRequestMethod());
            conn.setUseCaches(false);
            conn.setDoInput(true);
            if(mode != CONFIG)
                conn.setDoOutput(true);
            return conn;
        }

        public void addPostValue(String key,String value){
            postContent.add(new Pair<String,String>(key,value));
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            String reponse = "";
            try {
                HttpURLConnection conn = openConnection();
                if(mode != CONFIG)
                    this.addPostValue("mode","json");
                if(PHPSESSID!=null)
                    conn.setRequestProperty("Cookie","PHPSESSID="+PHPSESSID+";");

                UUID uniqueKey = UUID.randomUUID();
                String filename = uniqueKey.toString();

                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary =  "*****";

                if(mode == UPLOADER)
                {
                    conn.setRequestProperty("Connection","Keep-Alive");
                    conn.setRequestProperty("Cache-Control","no-cache");
                    conn.setRequestProperty("Content-Type","multipart/form-data;boundary="+boundary);
                }

                if(mode != CONFIG) {
                    DataOutputStream writer = new DataOutputStream(conn.getOutputStream());
                    if (mode != UPLOADER)
                        writer.writeBytes(getQuery(this.postContent));

                    if (mode == UPLOADER) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        this.image.compress(Bitmap.CompressFormat.PNG, 50, stream);
                        writer.writeBytes(twoHyphens + boundary + lineEnd);
                        writer.writeBytes("Content-Disposition: form-data; name=\"inputfile\";filename=\"" + filename + ".png\"" + lineEnd);
                        writer.writeBytes("Content-Type: image/png" + lineEnd);
                        writer.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
                        writer.writeBytes("Content-Length: "+stream.toByteArray().length+lineEnd);
                        writer.writeBytes(lineEnd);
                        writer.write(stream.toByteArray());
                        writer.writeBytes(lineEnd);
                        writer.writeBytes(twoHyphens + boundary + lineEnd);
                        writer.flush();
                        writer.writeBytes("Content-Disposition: form-data; name=\"mode\"" + lineEnd);
                        writer.writeBytes(lineEnd);
                        writer.writeBytes("json" + lineEnd);
                        writer.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    }

                    writer.flush();
                    writer.close();
                }

                int reponseCode = conn.getResponseCode();
                Log.d(TAG,"ResponseCode : "+reponseCode);
                System.out.println(mode+" REPONSE_CODE : " + reponseCode);
                if(reponseCode == HttpURLConnection.HTTP_OK)
                {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while((line=br.readLine())!=null) {
                        Log.d(TAG, line);
                        reponse+=line;
                    }

                    String cookie_line = conn.getHeaderField("set-cookie");
                    if(cookie_line != null) {
                        String[] cookies = cookie_line.split(";");
                        for (int i = 0; i < cookies.length; i++) {
                            if (cookies[i].contains("PHPSESSID")) {
                                PHPSESSID = cookies[i].split("=")[1];
                                Log.d(TAG, "PHPSESSID = " + PHPSESSID);
                            }
                        }
                    }
                    Log.d(TAG,"REPONSE : "+reponse);
                    System.out.println(mode+" REPONSE : "+reponse);
                }else{
                    BufferedReader errorStream = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                    String err;
                    while((err = errorStream.readLine())!=null)
                        System.out.println(err);
                    reponse = "{ alert : \"danger\", message : \""+reponseCode+"\" }";
                }
            } catch (MalformedURLException e) {
                reponse = "{ alert : \"danger\", message : \" MalformedURLException : "+e.getMessage()+"\" }";
                e.printStackTrace();
            } catch (IOException ex) {
                reponse = "{ alert : \"danger\", message : \" IOException : "+ ex.getMessage()+"\" }";
                ex.printStackTrace();
            }

            try {
                return new JSONObject(reponse);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String getQuery(List<Pair<String,String>> content) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            result.append(URLEncoder.encode(content.get(0).first,"UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(content.get(0).second,"UTF-8"));

            for(int i = 1; i < content.size(); i++){
                result.append("&") ;
                result.append(URLEncoder.encode(content.get(i).first,"UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(content.get(i).second,"UTF-8"));
            }

            return result.toString();
        }

        @Override
        protected void onPostExecute(JSONObject result){
            super.onPostExecute(result);
            try{
                if(mode != CONFIG) {
                    String message = result.getString("message");
                    if (result.get("alert").equals("danger")) receiver.logError(message);
                    else if (result.get("alert").equals("success")) receiver.logSuccess(message);
                }else{
                    if(result!=null)
                        receiver.logJSON(result);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
