#!/bin/bash

function error {

	if [ $? -gt 0 ]; then
		echo $1;
		exit $ind_err;

	else
		ind_err=`expr $ind_err + 1`;
	fi
}


export ind_err=2;



#Test si l'utilisateur est root ou non

if [ `id -u` -gt  0 ]; then
	echo "Vous devez etre root pour executer ce programme !";
	exit 1;
fi


#Test si la carte wifi est inseree et compatible

lsusb | grep "RTL8188CUS" &> /dev/null
error "Aucune carte wifi compatible n'a été trouvée !";


#Test de la connexion internet

ping google.com -c 1 &> /dev/null
error "Aucune connexion internet valide n'a été détectée !";

echo "Tous les tests ok. Installation des paquets nécessaires en cours...";

#Installation des paquets necessaires

apt-get update &> /dev/null
apt-get -y install hostapd udhcpd 1> /dev/null

error "Echec de l'installation ! ";
echo "Installation reussie !";

echo "Création du fichier de configuration...";

cat <<EOF > /etc/hostapd/hostapd.conf 
interface=wlan0
driver=rtl871xdrv
ssid=PhotoParty
hw_mode=g
wme_enabled=1
ieee80211n=1
ht_capab=[HT40+][SHORT-GI-40][DSSS_CCK-40]
channel=1
wpa=3
wpa_passphrase=photoparty1234
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
auth_algs=3
macaddr_acl=0
wmm_enabled=1
EOF

error "Error lors de la création du fichier de configuration !";

echo "Fichier de configuration crée !";

sleep 5;

echo "Modification de la configuration des interfaces en cours. Une sauvegarde de l'ancienne configuration sera effectuée";


cp /etc/network/interfaces /etc/network/SAUVEGARDE_INTERFACES
error "Erreur lors de la sauvegarde du fichier de configuration des interfaces !";

cat <<EOF > /etc/network/interfaces
auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet dhcp

auto wlan0
allow-hotplug wlan0
iface wlan0 inet static
address 192.168.4.1
netmask 255.255.255.0
EOF

error "Erreur lors de la modification du fichier de configuration d'interface. Vous pouvez récuperer votre ancienne configuration dans le fichier /etc/network/SAUVEGARDE_INTERFACES";

echo "Le fichier de configuration a été correctement mis a jour ! ";

sleep 5;

echo "Mise a jour du logiciel principal en cours. L'ancienne version du programme sera sauvegardée";

cp /usr/sbin/hostapd /usr/sbin/SAUVEGARDE_HOSTAPD
error "Erreur lors de la sauvegarde du logiciel principal. Vous pouvez récuperer l'ancien programme dans /usr/sbin/SAUVEGARDE_HOSTAPD";

rm -f /usr/sbin/hostapd
error "Erreur lors de la suppression de l'ancien logiciel ! ";

wget -P /usr/sbin/ http://mc.team-arma.fr/hostapd &> /dev/null
error "Erreaur lors de la récupération du nouveau logiciel !";

echo "Le nouveau programme s'est correctement installé. Finalisation des derniers paramètres...";

chown root:root /usr/sbin/hostapd
error "Erreur lors du changement du proprietaire du programme ! ";

chmod 755 /usr/sbin/hostapd
error "Erreur lors de l'attribution de permissions au programme ! ";

echo "DAEMON_CONF=\"/etc/default/hostapd.conf\"" >> /etc/default/hostapd
error "Erreur lors de la modification du fichier /etc/default/hostapd ! ";

sudo sed -i -e '$i sudo hostapd -B -P /run/hostapd.pid /etc/hostapd/hostapd.conf' /etc/rc.local

sleep 5;

echo "Mise en place du service dhcp en cours...";

cat <<EOF > /etc/udhcpd.conf
start	 192.168.4.2
end	 192.168.4.250

interface	wlan0

opt	dns	192.168.4.1
option	subnet	255.255.255.0
opt	router	192.168.4.1
opt	wins	192.168.4.1
option	domain	local
option lease	864000
EOF

cat<<EOF > /etc/default/udhcpd
DHCPD_ENABLED="yes"
EOF
error "Erreur lors de l'écriture de la configuration dhcp ! ";

echo "Configuration dhcp terminée !";
sleep 5


echo "Installation terminée  ! Le programme va etre redemarré dans 5 secondes afin de procéder aux changements. A l'issue de ce redémarrage, le reseau wifi 'PhotoParty' avec pour mot de passe 'photoparty1234' sera opérationnel.";

sleep 8
sudo ifdown wlan0 &> /dev/null && sudo ifup wan0 &> /dev/null 
sudo hostapd -B -P /run/hostapd.pid /etc/hostapd/hostapd.conf &> /dev/null
sudo /etc/init.d/udhcpd restart &> /dev/null
sleep 2

exit 0;








