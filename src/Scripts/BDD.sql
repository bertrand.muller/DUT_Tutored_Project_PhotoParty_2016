CREATE TABLE IF NOT EXISTS User(
	ipAddress INTEGER PRIMARY KEY,
	name VARCHAR(64) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS Stockage(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	prefix VARCHAR(256),
	type VARCHAR(64) CHECK(type='CarteSD' or type='DisqueDur' or type='Cloud')
);

CREATE TABLE IF NOT EXISTS `Photographie` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`path`	VARCHAR(256),
	`width` INTEGER,
	`height` INTEGER,
	`hash`	BLOB(128) UNIQUE,
	`id_stockage`	INTEGER NOT NULL,
	`ip_user`	INTEGER NOT NULL,
	FOREIGN KEY(id_stockage) REFERENCES Stockage(id),
	FOREIGN KEY(ip_user) REFERENCES User(ipAddress)
);

CREATE TABLE IF NOT EXISTS Vote(
	photo_id INTEGER NOT NULL,
	ip INTEGER NOT NULL,
	avis CHAR NOT NULL,
	PRIMARY KEY(photo_id,ip),
	FOREIGN KEY(photo_id) REFERENCES Photographie(id),
	FOREIGN KEY(ip) REFERENCES User(ipAddress)
);

CREATE TABLE IF NOT EXISTS Configuration(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	config_value TEXT
);

INSERT INTO Stockage VALUES(NULL,'/var/www/uploads','CarteSD');

/* 
 * ADMIN_PSEUDO=1
 * ADMIN_PASSWORD=2 (salt = ,# devant #* derrière
 * STOCKAGE_SELECT=3 (1-Carte SD / 2-DisqueDur / 3-Cloud)
 * TRANSITION_TIME=4 (ms)
 * DIAPORAMA_STATE=5 diaporama (0-arrêt / 1-marche)
 * ETHERNET_STATE=6 ethernet (0-desactivé / 1-activé)
 * TRANSITION_MASK=7
 */
INSERT INTO Configuration VALUES(1,'admin');
INSERT INTO Configuration VALUES(2,'939c54d1714991a960a06f8c4bfe370a4e34729ba86d347b8f8370955874e646');
INSERT INTO Configuration VALUES(3,'1');
INSERT INTO Configuration VALUES(4,'15000');
INSERT INTO Configuration VALUES(5,'1');
INSERT INTO Configuration VALUES(6,'0');
INSERT INTO Configuration VALUES(7,'0');

