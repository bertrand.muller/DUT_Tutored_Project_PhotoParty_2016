package transitions;

import affichage.swing.ProjecteurSwing;

import java.util.Observable;

/**
 * Created by thomas on 23/02/16.
 */
public abstract class Transition extends Observable {

    protected ProjecteurSwing projecteurSwing;
    public float translateX,translateY;
    public float scaleX,scaleY;
    public float rotate;
    public boolean isPlaying;

    public Transition(
            ProjecteurSwing projecteurSwing,
            float translateX,
            float translateY,
            float scaleX,
            float scaleY,
            float rotate
    ){
        this.translateX = translateX;
        this.translateY = translateY;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.rotate = rotate;
        this.projecteurSwing = projecteurSwing;
    }

    public abstract void play();
}
