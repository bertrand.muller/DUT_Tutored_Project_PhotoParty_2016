package affichage;

import modeles.Photographie;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by thomas on 05/03/16.
 */
public abstract class Projecteur{

    protected Galerie galerie;
    protected Photographie photo_courante;

    protected int width;
    protected int height;


    public Projecteur(){
        this.width = 1920;
        this.height = 1080;
    }

    public abstract void initWindows();

    public abstract void menuMode();
    public abstract void diaporamaMode();
    public abstract void refreshVote(int scoreVote);
    public abstract void preload(Photographie photo);
    public abstract void supprimerPhotographie(Photographie photographie);

    public void updatePhotographie(){
        Photographie nouvellePhoto = galerie.photographieCourante();
        if(this.photo_courante != nouvellePhoto){
            this.photo_courante = nouvellePhoto;
            if(this.photo_courante == null)
                this.menuMode();
            else
                this.diaporamaMode();
        }
    }

    public int getWidth(){
        return this.width;
    }

    public int getHeight(){
        return this.height;
    }
    public void attachGalerie(Galerie galerie){
        this.galerie = galerie;
    }
}
