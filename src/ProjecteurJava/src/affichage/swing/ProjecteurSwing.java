package affichage.swing;

import affichage.Projecteur;
import modeles.Photographie;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ProjecteurSwing extends Projecteur {

    private JFrame window;
    private DiaporamaSwing diaporamaSwing;

    @Override
    public void initWindows() {
        this.diaporamaSwing = new DiaporamaSwing();
        this.window = new JFrame();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        width = (int)dim.getWidth();
        height = (int)dim.getHeight();
        this.window.setUndecorated(true);
        this.window.setBounds(0,0, (int) dim.getWidth(), (int) dim.getHeight());
        this.window.setContentPane(diaporamaSwing);
        this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.window.setVisible(true);
        this.window.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
                    System.exit(0);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    @Override
    public void menuMode() {
        this.diaporamaSwing.setRenderMode(DiaporamaSwing.MENU);
    }

    @Override
    public void diaporamaMode(){
        this.diaporamaSwing.setPhotoToDisplay(photo_courante);
        this.diaporamaSwing.setRenderMode(DiaporamaSwing.DIAPORAMA);
        this.diaporamaSwing.repaint();
    }

    @Override
    public void refreshVote(int scoreVote){
        this.diaporamaSwing.repaint();
    }

    @Override
    public void preload(Photographie photo) {

    }

    @Override
    public void supprimerPhotographie(Photographie photographie) {

    }
}
