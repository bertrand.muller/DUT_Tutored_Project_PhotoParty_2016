package affichage.swing;

import modeles.Photographie;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by thomas on 05/03/16.
 */
public class DiaporamaSwing extends JPanel {
    public static final int MENU=0,DIAPORAMA=1;

    private BufferedImage [] icons;
    private BufferedImage logo;

    private int renderMode;

    private Photographie photoToDisplay;

    public DiaporamaSwing() {
        this.renderMode = MENU;
        this.icons = new BufferedImage[4];
        try {
            this.icons[0] = ImageIO.read(new File("res/bof.png"));
            this.icons[1] = ImageIO.read(new File("res/sympa.png"));
            this.icons[2] = ImageIO.read(new File("res/cool.png"));
            this.icons[3] = ImageIO.read(new File("res/aime.png"));
            this.logo = ImageIO.read(new File("res/logo.png"));
        } catch (IOException e) {
            System.err.println("Ressource introuvable : " + e.getMessage());
            System.exit(1);
        }
    }

    public void setPhotoToDisplay(Photographie photoToDisplay) {
        this.photoToDisplay = photoToDisplay;
    }

    public void setRenderMode(int renderMode) {
        this.renderMode = renderMode;
    }

    /**
     * Rendu de l'image
     * @param g
     */
    protected void paintComponent(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        switch (renderMode)
        {
            case DIAPORAMA:
                this.renderDiaporama(g2d);
            break;
            case MENU:
                this.renderMenu(g2d);
            break;
        }
    }

    private void renderDiaporama(Graphics2D g2d){
        photoToDisplay.chargerImage();
        BufferedImage img = photoToDisplay.getImage();
        double scalefactorWidth = (double) getWidth() / img.getWidth();
        double scalefactorHeight = (double) getHeight() / img.getHeight();
        double scalefactor = 1;

        if (scalefactorWidth < 1) {
            if (scalefactorHeight < scalefactorWidth) {
                scalefactor = scalefactorHeight;
            }
        }

        if (scalefactorHeight < 1) {
            if (scalefactorWidth < scalefactorHeight) {
                scalefactor = scalefactorWidth;
            }
        }

        g2d.setColor(Color.WHITE);
        BufferedImage currentIcon = this.icons[(int)(photoToDisplay.scoreVote())];
        AffineTransform save = g2d.getTransform();
        g2d.translate(getWidth()/2,getHeight()/2);
        g2d.scale(scalefactor,scalefactor);
        g2d.drawImage(img,-img.getWidth()/2,-img.getHeight()/2, this);
        g2d.setTransform(save);
        g2d.drawImage(currentIcon,getWidth()-75,getHeight()-75,75,75,null);
    }

    private void renderMenu(Graphics2D g2d){
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0,0,getWidth(),getHeight());
        g2d.drawImage(logo,getWidth()/2 - logo.getWidth()/2,getHeight()/2 - logo.getHeight()/2,null);
    }
}
