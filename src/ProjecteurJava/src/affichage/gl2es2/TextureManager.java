package affichage.gl2es2;

import java.io.File;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import modeles.Photographie;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.Buffer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thomas on 09/03/16.
 */
public class TextureManager {

    private DiaporamaContext context;
    private int GL_TextureXStart;
    private int textureUnitOffset;


    private HashMap<Integer,Photographie> textureUnit_photographie;

    public TextureManager(DiaporamaContext context,int GL_TextureXStart){
        this.textureUnit_photographie = new HashMap<>();
        this.context = context;
        this.GL_TextureXStart  = GL_TextureXStart;
        this.textureUnitOffset = GL_TextureXStart - GL.GL_TEXTURE0;
    }

    public synchronized boolean loadPhotographie(GL2ES2 gl,Photographie photographie){
        /* Si la photographie n'est pas chargé */
        if(!this.textureUnit_photographie.containsValue(photographie))
        {
            /* On récupère l'id d'une zone pour texture libre */
            int textureUnit = this.getTextureUnitFree();
            if (textureUnit != -1) {
                System.out.println("[TEXTURE MANAGER] load texture at "+textureUnit);
                //loadPhotographie(gl, photographie, GL.GL_TEXTURE0 + textureUnit, textureUnit);
                this.loadFastPhotographie(gl,photographie,GL.GL_TEXTURE0 + textureUnit,textureUnit);
                this.textureUnit_photographie.put(textureUnit,photographie);
            }else
                return false;
        }
        return true;
    }

    public void free(Photographie photo){
        this.textureUnit_photographie.remove(photo);
    }

    public void reset(){
        this.textureUnit_photographie.clear();
    }

    public int getTextureUnit(Photographie photographie){
        for(Map.Entry<Integer,Photographie> entry : this.textureUnit_photographie.entrySet()){
            if(entry.getValue() == photographie)
                return entry.getKey();
        }
        return this.textureUnitOffset - 1;
    }

    @Deprecated
    public void loadPhotographie(GL2ES2 gl,Photographie photographie,int GL_TEXTUREX,int textureUnit){
        photographie.chargerImage();
        BufferedImage image = photographie.getImage();
        gl.glActiveTexture(GL_TEXTUREX);
        long d = System.currentTimeMillis(); //Calcul temps de chargement

        //Conversion de l'image en ARGB
        BufferedImage imgnew = context.createImageFromScreenSize(image);

        TextureData tx_data = AWTTextureIO.newTextureData(gl.getGLProfile(),imgnew,false);
        Buffer pix_buffer = tx_data.getBuffer();

        //Configuration texture
        gl.glBindTexture(GL2ES2.GL_TEXTURE_2D,context.getTexturenames()[textureUnit]);
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR);
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR);
        gl.glTexImage2D(
                GL2ES2.GL_TEXTURE_2D,0,
                GL2ES2.GL_RGBA,tx_data.getWidth(),tx_data.getHeight(),
                0, GL2ES2.GL_RGBA, GL2ES2.GL_UNSIGNED_BYTE,pix_buffer);
        System.out.println("Texture loaded in "+(System.currentTimeMillis() - d)+" ms");
        System.out.println("Texture width : "+tx_data.getWidth());
        System.out.println("Texture height : "+tx_data.getHeight());

        pix_buffer = null;
        tx_data = null;
        imgnew = null;
        photographie.dechargerImage();
        Runtime.getRuntime().gc();
    }

    public void loadFastPhotographie(GL2ES2 gl,Photographie photographie,int GL_TEXTUREX,int textureUnit){
        try {
            long d = System.currentTimeMillis(); //Calcul temps de chargement
            TextureData tx_data = TextureIO.newTextureData(gl.getGLProfile(), new File(photographie.getChemin()),GL2ES2.GL_RGB,GL2ES2.GL_RGB, false, TextureIO.PNG);
            System.out.println("Texture width : "+tx_data.getWidth());
            System.out.println("Texture height : "+tx_data.getHeight());
            Buffer pix_buffer = tx_data.getBuffer();
            pix_buffer.rewind();
            gl.glActiveTexture(GL_TEXTUREX);
            //Configuration texture
            gl.glBindTexture(GL2ES2.GL_TEXTURE_2D,context.getTexturenames()[textureUnit]);
            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR);
            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR);
            try {
                gl.glTexImage2D(
                        GL2ES2.GL_TEXTURE_2D, 0,
                        GL2ES2.GL_RGB, tx_data.getWidth(), tx_data.getHeight(),
                        0, GL2ES2.GL_RGB, GL2ES2.GL_UNSIGNED_BYTE, pix_buffer);
            }catch(GLException e){
                System.out.println("Erreur de chargement essais n°2");
                this.loadPhotographie(gl,photographie,GL_TEXTUREX,textureUnit);
            }
            System.out.println("Texture loaded in "+(System.currentTimeMillis() - d)+" ms");
        }catch(IOException e){
            System.out.println("Failed to load texture");
        }
    }

    public int getTextureUnitFree(){
        int textureUnit = -1;
        for(int k = this.textureUnitOffset; k < ProjecteurGL2ES2.N_TEXTURE_MAX - 1; k++)
        {
            if(!this.textureUnit_photographie.keySet().contains(k)){
                textureUnit = k;
                break;
            }
        }

        return textureUnit;
    }
}
