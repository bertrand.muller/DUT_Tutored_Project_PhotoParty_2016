package affichage.gl2es2.shapes;

import affichage.gl2es2.GL2ES2Context;
import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.util.PMVMatrix;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by thomas on 08/03/16.
 */
public abstract class Shape {

    protected GL2ES2Context context;
    protected int [] vboHandles;
    protected float[] vertices;
    protected float[] texCoords;
    protected short[] drawOrder;

    protected FloatBuffer verticesBuffer;
    protected FloatBuffer texCoordsBuffer;
    protected ShortBuffer drawOrderBuffer;

    protected int verticesBufferSize;
    protected int texCoordsBufferSize;
    protected int drawOrderBufferSize;

    protected int textureUnit;
    protected Matrix4 transformMatrix;
    protected float[] resultMatrix;

    public Shape(GL2ES2Context context){
        this.context = context;
        initCoords();
        this.resultMatrix = new float[16];
        this.transformMatrix = new Matrix4();
        this.transformMatrix.loadIdentity();
        this.textureUnit = 0;
        this.verticesBuffer = Buffers.newDirectFloatBuffer(vertices);
        this.verticesBuffer.position(0);
        this.verticesBufferSize = this.vertices.length * 4;
        this.texCoordsBuffer= Buffers.newDirectFloatBuffer(texCoords);
        this.texCoordsBuffer.position(0);
        this.texCoordsBufferSize = this.texCoords.length * 4;

        if(this.drawOrder != null) {
            this.drawOrderBuffer = Buffers.newDirectShortBuffer(drawOrder);
            this.drawOrderBuffer.position(0);
            this.drawOrderBufferSize = this.drawOrder.length * 4;
        }
    }

    public void init(GL2ES2 gl){
        this.vboHandles = context.getVboHandles();
        gl.glEnableVertexAttribArray(GL2ES2Context.ATTRIBUTE_POSITION);
        gl.glEnableVertexAttribArray(GL2ES2Context.ATTRIBUTE_TEXCOORD);
        this.transfertVerticesToGPU(gl);
        this.transfertTexCoordsToGPU(gl);
        gl.glDisableVertexAttribArray(GL2ES2Context.ATTRIBUTE_POSITION);
        gl.glDisableVertexAttribArray(GL2ES2Context.ATTRIBUTE_TEXCOORD);
    }

    public void transfertVerticesToGPU(GL2ES2 gl){
        gl.glBindBuffer(GL2ES2.GL_ARRAY_BUFFER,this.vboHandles[GL2ES2Context.VERTICES_IDX]);
        gl.glBufferData(GL.GL_ARRAY_BUFFER,getVerticesBufferSize(),this.verticesBuffer,GL.GL_STATIC_DRAW);
        gl.glVertexAttribPointer(GL2ES2Context.ATTRIBUTE_POSITION /* the vertex attribute */,
                3,
                GL2ES2.GL_FLOAT, false /* normalized? */,
                0 /* stride */,
                0 /* The bound VBO data offset */);
    }

    public void transfertTexCoordsToGPU(GL2ES2 gl){
        gl.glBindBuffer(GL2ES2.GL_ARRAY_BUFFER,this.vboHandles[GL2ES2Context.TEXCOORD_IDX]);
        gl.glBufferData(GL.GL_ARRAY_BUFFER,this.getTexCoordsBufferSize(),this.texCoordsBuffer,GL.GL_STATIC_DRAW);
        gl.glVertexAttribPointer(GL2ES2Context.ATTRIBUTE_TEXCOORD,2, GL2ES2.GL_FLOAT,false,0,0);
    }

    public abstract void render(GL2ES2 gl);
    public abstract float[] update(PMVMatrix pmv,double delta);

    public void setTextureUnit(int textureUnit){
        this.textureUnit = textureUnit;
    }

    public int getTextureUnit(){
        return this.textureUnit;
    }

    public abstract void initCoords();

    public FloatBuffer getVerticesBuffer(){
        return this.verticesBuffer;
    }

    public FloatBuffer getTexCoordsBuffer(){
        return this.texCoordsBuffer;
    }

    public ShortBuffer getDrawOrderBuffer(){
        return this.drawOrderBuffer;
    }

    public int getVerticesBufferSize(){ return this.vertices.length * 4; }

    public int getTexCoordsBufferSize(){
        return this.texCoords.length * 4;
    }

    public int getDrawOrderBufferSize(){
        return this.drawOrder.length * 4;
    }

    public void setTransformMatrix(Matrix4 matrix){
        this.transformMatrix = matrix;
    }

    public Matrix4 getTransformMatrix(){
        return this.transformMatrix;
    }
}
