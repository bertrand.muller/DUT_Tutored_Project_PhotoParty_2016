package affichage.gl2es2.shapes;

import affichage.gl2es2.DiaporamaContext;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.util.PMVMatrix;

/**
 * Created by thomas on 08/03/16.
 */
public class TexturedPlane extends Shape
{

    public TexturedPlane(DiaporamaContext context) {
        super(context);
    }

    @Override
    public void init(GL2ES2 gl){
        super.init(gl);
        transfertDrawOrderToGPU(gl);
    }

    public void transfertDrawOrderToGPU(GL2ES2 gl){
        gl.glBindBuffer(GL2ES2.GL_ELEMENT_ARRAY_BUFFER,this.vboHandles[DiaporamaContext.DRAWLIST_IDX]);
        gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER,this.getDrawOrderBufferSize(),this.drawOrderBuffer,GL.GL_STATIC_DRAW);
    }

    @Override
    public void render(GL2ES2 gl)
    {
        gl.glUniformMatrix4fv(context.getmHandleMVPMatrix(),1,false,this.resultMatrix,0);
        this.transfertVerticesToGPU(gl);
        gl.glEnableVertexAttribArray(DiaporamaContext.ATTRIBUTE_POSITION);
        this.transfertTexCoordsToGPU(gl);
        gl.glEnableVertexAttribArray(DiaporamaContext.ATTRIBUTE_TEXCOORD);
        this.transfertDrawOrderToGPU(gl);
        gl.glDepthMask(false);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA,GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glUniform1i(context.getmHandleTexture(),this.textureUnit);
        gl.glDrawElements(GL.GL_TRIANGLES,6, GL.GL_UNSIGNED_SHORT,0);
        gl.glDisableVertexAttribArray(DiaporamaContext.ATTRIBUTE_POSITION);
        gl.glDisableVertexAttribArray(DiaporamaContext.ATTRIBUTE_TEXCOORD);

        gl.glDisable(GL.GL_BLEND);
        gl.glDepthMask(true);
    }

    @Override
    public float[] update(PMVMatrix pmv, double delta) {
        pmv.glPushMatrix();
        pmv.glMultMatrixf(this.transformMatrix.getMatrix(),0);
        pmv.multMvPMatrixf(this.resultMatrix,0);
        pmv.glPopMatrix();
        return this.resultMatrix;
    }

    @Override
    public void initCoords() {
        this.vertices = new float[]{
                -1f,  1f, 0.0f,
                -1f, -1f, 0.0f,
                1f, -1f, 0.0f,
                1f,  1f, 0.0f,
        };

        this.texCoords = new float []{
                0.0f,0.0f,
                0.0f,1.0f,
                1.0f,1.0f,
                1.0f,0.0f
        };

        this.drawOrder = new short[] {0,1,2,0,2,3};
    }
}
