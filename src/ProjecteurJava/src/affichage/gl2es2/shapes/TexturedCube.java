package affichage.gl2es2.shapes;

import affichage.gl2es2.GL2ES2Context;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.util.PMVMatrix;

/**
 * Created by thomas on 08/03/16.
 */
public class TexturedCube extends Shape
{

    public TexturedCube(GL2ES2Context context) {
        super(context);
    }

    @Override
    public void render(GL2ES2 gl)
    {
        gl.glUniformMatrix4fv(this.context.getmHandleMVPMatrix(),1,false,this.resultMatrix,0);
        gl.glEnableVertexAttribArray(GL2ES2Context.ATTRIBUTE_POSITION);
        gl.glEnableVertexAttribArray(GL2ES2Context.ATTRIBUTE_TEXCOORD);
        gl.glUniform1i(this.context.getmHandleTexture(),this.textureUnit);
        gl.glDrawArrays(GL2ES2.GL_TRIANGLES, 0, 36);
        gl.glDisableVertexAttribArray(GL2ES2Context.ATTRIBUTE_POSITION);
        gl.glDisableVertexAttribArray(GL2ES2Context.ATTRIBUTE_TEXCOORD);
    }

    @Override
    public float[] update(PMVMatrix pmv, double delta) {
        pmv.glPushMatrix();
        this.transformMatrix.loadIdentity();
        this.transformMatrix.rotate((float)Math.toRadians(delta),0.0f,1.0f,0.0f);
        pmv.glMultMatrixf(this.transformMatrix.getMatrix(),0);
        pmv.multMvPMatrixf(this.resultMatrix,0);
        pmv.glPopMatrix();
        return this.resultMatrix;
    }

    @Override
    public void initCoords() {
        this.vertices = new float[]
                {

                        // Front face
                        -1.0f, 1.0f, 1.0f,
                        -1.0f, -1.0f, 1.0f,
                        1.0f, 1.0f, 1.0f,
                        -1.0f, -1.0f, 1.0f,
                        1.0f, -1.0f, 1.0f,
                        1.0f, 1.0f, 1.0f,

                        // Right face
                        1.0f, 1.0f, 1.0f,
                        1.0f, -1.0f, 1.0f,
                        1.0f, 1.0f, -1.0f,
                        1.0f, -1.0f, 1.0f,
                        1.0f, -1.0f, -1.0f,
                        1.0f, 1.0f, -1.0f,

                        // Back face
                        1.0f, 1.0f, -1.0f,
                        1.0f, -1.0f, -1.0f,
                        -1.0f, 1.0f, -1.0f,
                        1.0f, -1.0f, -1.0f,
                        -1.0f, -1.0f, -1.0f,
                        -1.0f, 1.0f, -1.0f,

                        // Left face
                        -1.0f, 1.0f, -1.0f,
                        -1.0f, -1.0f, -1.0f,
                        -1.0f, 1.0f, 1.0f,
                        -1.0f, -1.0f, -1.0f,
                        -1.0f, -1.0f, 1.0f,
                        -1.0f, 1.0f, 1.0f,

                        // Top face
                        -1.0f, 1.0f, -1.0f,
                        -1.0f, 1.0f, 1.0f,
                        1.0f, 1.0f, -1.0f,
                        -1.0f, 1.0f, 1.0f,
                        1.0f, 1.0f, 1.0f,
                        1.0f, 1.0f, -1.0f,

                        // Bottom face
                        1.0f, -1.0f, -1.0f,
                        1.0f, -1.0f, 1.0f,
                        -1.0f, -1.0f, -1.0f,
                        1.0f, -1.0f, 1.0f,
                        -1.0f, -1.0f, 1.0f,
                        -1.0f, -1.0f, -1.0f
                };

        this.texCoords = new float[]
                {
                        // Front face
                        0.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f,

                        // Right face
                        0.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f,

                        // Back face
                        0.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f,

                        // Left face
                        0.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f,

                        // Top face
                        0.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f,

                        // Bottom face
                        0.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f
                };
    }
}
