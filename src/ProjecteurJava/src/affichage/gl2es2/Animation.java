package affichage.gl2es2;

import com.jogamp.opengl.math.Matrix4;
import modeles.Configuration;

import java.util.ArrayList;

/**
 * Created by thomas on 12/03/16.
 */
@Deprecated
public class Animation {

    public static final int ROTATION_Z_ZOOM=0,ROTATION_Y_ZOOM=1,ROTATION_Y=2;

    private static final int N_TRANSITION=2;
    private float x,y,z;
    private float rx,ry,rz;
    private float sx,sy;

    private float dx,dy,dz;
    private float rdx,rdy,rdz;
    private float sdx,sdy;

    private float ax,ay,az;
    private long timeInFrame;
    private long startFrame;

    private Matrix4 state;

    private ArrayList<Integer> ids;

    public Animation(long timeInFrame){
        this.state = new Matrix4();
        this.timeInFrame = timeInFrame;
        this.ids = new ArrayList<>();
    }

    public boolean isStop(){
        return this.startFrame == this.timeInFrame;
    }

    public double getRY(){
        return this.ry;
    }

    public void update(){
        if(this.startFrame < this.timeInFrame){
            this.dx+=this.ax;
            this.dy+=this.ay;
            this.dz+=this.az;
            this.x+=this.dx;
            this.y+=this.dy;
            this.z+=this.dz;
            this.rx+=this.rdx;
            this.ry+=this.rdy;
            this.rz+=this.rdz;
            this.sx+=this.sdx;
            this.sy+=this.sdy;
            this.loadState();
            this.startFrame++;
        }
    }

    public Matrix4 getState(){
        return this.state;
    }

    public void reset(){
        this.startFrame = 0;
        int transition_mask = Configuration.getInstance().getTransitionMask();
        this.ids.clear();
        for(int i = 0; i < N_TRANSITION; i++) {
            if((transition_mask & (1 << i))!=0) {
                this.ids.add(i);
            }
        }
    }

    private void loadState(){
        this.state.loadIdentity();
        this.state.translate(x,y,z);
        this.state.rotate(rx,1,0,0);
        this.state.rotate(ry,0,1,0);
        this.state.rotate(rz,0,0,1);
        this.state.scale(sx,sy,1f);
    }


    public void setStartPosition(float x,float y,float z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setStartScale(float sx,float sy){
        this.sx = sx;
        this.sy = sy;
    }

    public void setStartRotation(float rx,float ry,float rz){
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }


    public void setUniformTranslation(float x,float y,float z){
        this.dx = (x - this.x)/timeInFrame;
        this.dy = (y - this.y)/timeInFrame;
        this.dz = (z - this.z)/timeInFrame;
    }

    public void setUniformScale(float sx,float sy){
        this.sdx = (sx - this.sx)/timeInFrame;
        this.sdy = (sy - this.sy)/timeInFrame;
    }

    public void setUniformRotation(float rx,float ry,float rz){
        this.rdx = (rx - this.rx)/timeInFrame;
        this.rdy = (ry - this.ry)/timeInFrame;
        this.rdz = (rz - this.rz)/timeInFrame;
    }

    public void setSpeedX(float dx,float ax){
        this.dx = dx;
        this.ax = ax;
    }

    public void setSpeedY(float dy,float ay){
        this.dy = dy;
        this.ay = ay;
    }

    public void setSpeedZ(float dz,float az){
        this.dz = dz;
        this.az = az;
    }

    public void loadRandomTransition(){
        this.reset();
        if(this.ids.size() > 1) {
            int rnd = (int) (Math.random() * this.ids.size());
            this.loadTransition(this.ids.get(rnd));
        }else if(this.ids.size() == 1){
            this.loadTransition(this.ids.get(0));
        }else{
            this.loadTransition(0);
            this.startFrame = timeInFrame;
        }
    }

    public void loadTransition(int id){
        System.out.println("[LOAD TRANSITION] id : "+id);
        /* Chargement des paramètres par défaut */
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.rx = 0;
        this.ry = 0;
        this.rz = 0;
        this.sx = 1;
        this.sy = 1;

        switch(id){
            case ROTATION_Z_ZOOM:
                this.setStartPosition(0,0,-4f);
                this.setStartRotation(0,0,0);
                this.setUniformTranslation(0,0,0f);
                this.setUniformRotation(0,0,(float)Math.toRadians(360f));
                break;
            case ROTATION_Y_ZOOM:
                this.setStartPosition(0,0,-2f);
                this.setUniformTranslation(0,0,0);
                this.setStartRotation(0,(float)Math.toRadians(-360f),0);
                this.setUniformRotation(0,0,0);
                break;
            case ROTATION_Y:
                this.setStartPosition(0,0,0f);
                this.setUniformTranslation(0,0,0);
                this.setStartRotation(0,(float)Math.toRadians(-360f),0);
                this.setUniformRotation(0,0,0);
                this.setStartScale(0.2f,0.2f);
                this.setUniformScale(0.2f,0.2f);
            break;
        }
    }
}
