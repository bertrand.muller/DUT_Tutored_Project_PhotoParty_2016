package affichage.gl2es2;

import affichage.gl2es2.animation.Transition;
import modeles.Configuration;

import java.util.ArrayList;

/**
 * Created by thomas on 29/03/16.
 */
public class TransitionManager {

    private ArrayList<Transition> transitionList;

    public TransitionManager(){
        this.transitionList = new ArrayList<>();
        this.updateAnimationList();
    }

    public void updateAnimationList(){
        this.transitionList.clear();
        int mask = Configuration.getInstance().getTransitionMask();
        for(int i = 0; i < Transition.N_TRANSITION; i++){
            if((mask & (1 << i))!=0) {
                this.transitionList.add(Transition.getTransition(i));
            }
        }
    }

    public Transition getAnimation(){
        if(this.transitionList.size() > 1){
            int id = (int)(Math.random()*(double)this.transitionList.size());
            return this.transitionList.get(id);
        }else if(this.transitionList.size()==1){
            return this.transitionList.get(0);
        }else{
            return Transition.getDefaultTransition();
        }
    }
}
