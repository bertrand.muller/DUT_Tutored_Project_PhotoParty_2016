package affichage.gl2es2;

import affichage.gl2es2.animation.*;
import affichage.gl2es2.shapes.TexturedPlane;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.util.PMVMatrix;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import modeles.Photographie;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.Buffer;

/**
 * Created by thomas on 02/03/16.
 */
public class DiaporamaContext extends GL2ES2Context {
    public static final int DRAWLIST_IDX = 2;

    private Matrix4 scaleMatrix;

    private static final int BOF=0,SYMPA=1,COOL=2,AIME=3;

    private int resolution_width;
    private int resolution_height;
    private int score;

    private TexturedPlane cadre;
    private TexturedPlane smiley;
    private Transition transitionPhotographie;
    private AnimationRotation animation_smiley;

    private TextureManager textureManager;
    private TransitionManager transitionManager;

    private Matrix4 smileyMatrix;
    private PMVMatrix smileyProjection;

    private float ratio_w,ratio_h;

    public DiaporamaContext(){
        this.textureManager = new TextureManager(this,GL.GL_TEXTURE4);
        this.transitionManager = new TransitionManager();
        this.scaleMatrix = new Matrix4();
        this.smileyProjection = new PMVMatrix();
        this.smileyProjection.glLoadIdentity();
        this.cadre = new TexturedPlane(this);
        this.smiley = new TexturedPlane(this);
        this.setResolution(320,240);

        this.transitionPhotographie = this.transitionManager.getAnimation();
        this.animation_smiley = new AnimationRotation(
                new Identity(),
                50,
                new Vector3D(0,1,0),0f,(float)Math.toRadians(180)
        );
    }

    @Override
    public void init(GL2ES2 gl){
        super.init(gl);
        this.cadre.init(gl);
        this.smiley.init(gl);
        smileyMatrix = new Matrix4();
        this.smileyMatrix.loadIdentity();
        this.smiley.setTransformMatrix(smileyMatrix);
    }

    public void setResolution(int width,int height){
        this.resolution_width = width;
        this.resolution_height = height;
        if(this.resolution_width > this.resolution_height) {
            ratio_w = (float)this.resolution_width/this.resolution_height;
            ratio_h = 1;
        }else{
            ratio_w = 1;
            ratio_h = (float)this.resolution_height/this.resolution_width;
        }
    }

    public BufferedImage createImageFromScreenSize(BufferedImage convert){
        BufferedImage image = new BufferedImage(this.resolution_width,this.resolution_height,BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = image.createGraphics();
        double scalefactorWidth = (double)this.resolution_width / convert.getWidth();
        double scalefactorHeight = (double)this.resolution_height / convert.getHeight();
        double scalefactor = 1;

        if (scalefactorWidth < 1) {
            if (scalefactorHeight < scalefactorWidth) {
                scalefactor = scalefactorHeight;
            }
        }

        if (scalefactorHeight < 1) {
            if (scalefactorWidth < scalefactorHeight) {
                scalefactor = scalefactorWidth;
            }
        }

        g2d.translate(this.resolution_width/2,this.resolution_height/2);
        g2d.scale(scalefactor,scalefactor);
        g2d.drawImage(convert,-convert.getWidth()/2,-convert.getHeight()/2,null);
        g2d.dispose();
        return image;
    }

    public void preloadPhotographie(GL2ES2 gl, Photographie photo){
        System.out.println("[PRELOAD]");
        this.textureManager.loadPhotographie(gl,photo);
    }

    public void afficherPhotographie(GL2ES2 gl, Photographie photo){
        this.setVote((int) photo.scoreVote());
        if(!this.textureManager.loadPhotographie(gl,photo)) {
            this.textureManager.loadFastPhotographie(gl,photo,GL.GL_TEXTURE0 +  ProjecteurGL2ES2.N_TEXTURE_MAX - 1,ProjecteurGL2ES2.N_TEXTURE_MAX - 1);
            this.cadre.setTextureUnit(ProjecteurGL2ES2.N_TEXTURE_MAX - 1);
        }else {
            this.cadre.setTextureUnit(this.textureManager.getTextureUnit(photo));
        }
        this.updateScaleMatrix(photo.getWidth(),photo.getHeight());
        this.transitionManager.updateAnimationList();
        this.transitionPhotographie = this.transitionManager.getAnimation();
        this.transitionPhotographie.start();
        System.out.println(transitionPhotographie);
    }

    public void updateScaleMatrix(int width,int height){
        this.scaleMatrix.loadIdentity();
        float ratio = (float)width/height;
        if(ratio < 1.0f) // height > width
        {
            this.scaleMatrix.scale(ratio,-1f,1f);
        }else //width > height
        {
            this.scaleMatrix.scale(1f,-1f/ratio,1f);
        }

        this.cadre.setTransformMatrix(scaleMatrix);
    }

    protected void loadIcon(GL2ES2 gl,int textureUnit,int textureIDX,String fileName) throws IOException {
        TextureData data = AWTTextureIO.newTextureData(gl.getGLProfile(),ImageIO.read(new File(fileName)),false);
        //TextureData data = TextureIO.newTextureData(gl.getGLProfile(),new File(fileName),false, TextureIO.PNG);
        Buffer pix_buffer = data.getBuffer();
        //Configuration texture
        gl.glActiveTexture(textureUnit);
        gl.glBindTexture(GL2ES2.GL_TEXTURE_2D,texturenames[textureIDX]);
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR);
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR);
        gl.glTexImage2D(
                GL2ES2.GL_TEXTURE_2D,0,
                GL2ES2.GL_RGBA,data.getWidth(),data.getHeight(),
                0, GL2ES2.GL_RGBA, GL2ES2.GL_UNSIGNED_BYTE,pix_buffer);

        pix_buffer = null;
        data = null;

        Runtime.getRuntime().gc();
    }

    protected void initTexture(GL2ES2 gl){
        super.initTexture(gl);
        try {
            System.out.println("[INIT TEXTURE]");
            this.cadre.setTextureUnit(4);
            this.loadIcon(gl,GL.GL_TEXTURE0,BOF,"res/bof.png");
            this.loadIcon(gl,GL.GL_TEXTURE1,SYMPA,"res/sympa.png");
            this.loadIcon(gl,GL.GL_TEXTURE2,COOL,"res/cool.png");
            this.loadIcon(gl,GL.GL_TEXTURE3,AIME,"res/aime.png");
            Runtime.getRuntime().gc();
        } catch (FileNotFoundException e) {
            System.err.println("initTexture ERROR : "+e.getMessage());
        } catch (IOException e) {
            System.err.println("initTexture ERROR : "+e.getMessage());
        }
    }


    @Override
    public void render(GL2ES2 gl, PMVMatrix pmv, double delta) {
        gl.glUseProgram(shaderProgram);


        if(this.transitionPhotographie.isStart()) {
            this.transitionPhotographie.update();
            //Matrix4 finalTransformation = this.transitionPhotographie.getState();
            //Matrix4 finalTransformation = this.transitionPhotographie.getMatrix();
            Matrix4 source = this.transitionPhotographie.getMatrix();
            source.multMatrix(this.scaleMatrix);
            this.cadre.setTransformMatrix(source);
        }

        this.cadre.update(pmv,delta);
        this.cadre.render(gl);


        if(this.animation_smiley.isStart()){
            this.animation_smiley.update();
            //double ry = this.animation_smiley.getRY();
            long time = this.animation_smiley.getTime();
            if(time > 25)
                this.smiley.setTextureUnit(score);
            //Matrix4 finalTransformation = this.animation_smiley.getState();
            Matrix4 finalTransformation = this.animation_smiley.getMatrix();
            this.smileyMatrix.loadIdentity();
            this.smileyMatrix.translate(0.85f,-0.85f,0f);
            this.smileyMatrix.scale(0.08f/ratio_w,0.08f/ratio_h,1);
            this.smileyMatrix.multMatrix(finalTransformation);
            this.smiley.setTransformMatrix(smileyMatrix);
        }

        this.smiley.update(this.smileyProjection,delta);
        this.smiley.render(gl);
    }

    public void setVote(int score){
        System.out.println("[DIAPORAMA] Mise a jour score : "+score);
        this.animation_smiley.start();
        this.score = score;
    }

    public void libererPhoto(Photographie photo){
        this.textureManager.free(photo);
    }

    @Override
    public int VBOCount()
    {
        return 3;
    }

    /*
     * 0 : BOF
     * 1 : SYMPA
     * 2 : COOL
     * 3 : AIME
     * 4 : CURRENT IMAGE
     * 5-N : PRELOAD
     */
    @Override
    public int TextureCount() {
        return ProjecteurGL2ES2.N_TEXTURE_MAX;
    }

    @Override
    public void release(GL2ES2 gl){
        super.release(gl);
        this.textureManager.reset();
    }
}
