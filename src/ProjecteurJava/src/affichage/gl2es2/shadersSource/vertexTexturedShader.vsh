#ifdef GL_ES
    precision mediump float;   // Precision Qualifiers
    precision mediump int;     // GLSL ES section 4.5.2
#endif

uniform mat4 uniform_Projection;  // Incomming data used by
attribute vec4 attribute_Position;  // the vertex shader

attribute vec2 a_texCoord;
varying vec2 v_texCoord;

void main(void)
{
    gl_Position = uniform_Projection * attribute_Position;
    v_texCoord = a_texCoord;
}