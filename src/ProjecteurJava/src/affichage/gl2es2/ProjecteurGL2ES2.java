package affichage.gl2es2;

import affichage.Projecteur;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.*;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.PMVMatrix;
import modeles.Photographie;

/**
 * Created by thomas on 04/03/16.
 */
public class ProjecteurGL2ES2 extends Projecteur implements GLEventListener {

    public static final int MENU=0,DIAPORAMA=1;
    public static final int RENDER=0,LOAD_IMAGE=1,PRELOAD_IMAGE=2;

    public static int N_TEXTURE_MAX;

    private boolean configurationChanged;
    private int renderMode;
    private int request;
    private PMVMatrix pmvMatrix;

    private Photographie photo_preload;


    private DiaporamaContext diapo;
    private LogoContext logo;

    private double t0;
    private double delta;

    public ProjecteurGL2ES2()
    {
        this.logo = new LogoContext();
        this.diapo = new DiaporamaContext();
        this.pmvMatrix = new PMVMatrix();
        this.t0 = System.currentTimeMillis();
        this.setRenderMode(MENU);
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2ES2 gl = drawable.getGL().getGL2ES2();

        System.out.println("Chosen GLCapabilities: " + drawable.getChosenGLCapabilities());
        System.out.println("INIT GL IS: " + gl.getClass().getName());
        System.out.println("GL_VENDOR: " + gl.glGetString(GL.GL_VENDOR));
        System.out.println("GL_RENDERER: " + gl.glGetString(GL.GL_RENDERER));
        System.out.println("GL_VERSION: " + gl.glGetString(GL.GL_VERSION));
        int [] recv = new int [1];
        gl.glGetIntegerv(GL2ES2.GL_MAX_TEXTURE_IMAGE_UNITS,recv,0);
        N_TEXTURE_MAX = recv[0];
        System.out.println("GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: "+ProjecteurGL2ES2.N_TEXTURE_MAX);
        gl.glEnable(GL.GL_DEPTH_TEST);
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        GL2ES2 gl = drawable.getGL().getGL2ES2();
        if(this.logo.isInit())
            this.logo.release(gl);
        if(this.diapo.isInit())
            this.diapo.release(gl);
        System.exit(0);
    }

    private void configure(GL2ES2 gl){
        System.out.println("[CONFIGURE] : "+this.renderMode);
        switch (this.renderMode){
            case MENU:
                if(this.diapo.isInit())
                    this.diapo.release(gl);
                this.logo.init(gl);
                gl.glEnable(GL.GL_CULL_FACE);
                this.setCamera(0f,2f,-3f);
                break;
            case DIAPORAMA:
                if(this.logo.isInit())
                    this.logo.release(gl);
                this.diapo.init(gl);
                gl.glDisable(GL.GL_CULL_FACE);
                this.setCamera(0f,0f,2f);
                this.diapo.setResolution(width,height);
                break;
        }
        this.configurationChanged = false;
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        double t1 = System.currentTimeMillis();
        delta += (t1 - t0) * 0.005f;
        t0 = t1;

        GL2ES2 gl = drawable.getGL().getGL2ES2();

        if(configurationChanged)
            configure(gl);


        gl.glClearColor(0, 0, 0, 1f);
        gl.glClear(
                GL2ES2.GL_COLOR_BUFFER_BIT   |
                        GL2ES2.GL_DEPTH_BUFFER_BIT   );

        if(!configurationChanged)
        {
            switch (renderMode) {
                case MENU:
                    logo.render(gl, pmvMatrix, delta * 10f);
                    break;
                case DIAPORAMA:
                    switch(request) {
                        case RENDER:
                            diapo.render(gl, pmvMatrix, delta);
                            break;
                        case LOAD_IMAGE:
                            diapo.afficherPhotographie(gl,photo_courante);
                            this.request = RENDER;
                            break;
                        case PRELOAD_IMAGE:
                            this.diapo.preloadPhotographie(gl, photo_preload);
                            diapo.render(gl, pmvMatrix, delta);
                            this.request = RENDER;
                            break;
                    }
                    break;
            }
        }
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
        System.out.println("Window resized to width=" + w + " height=" + h);
        width = w;
        height = h;
        this.diapo.setResolution(width,height);
        drawable.getGL().glViewport(0,0,w,h);
        configure(drawable.getGL().getGL2ES2());
    }

    private void setCamera(float eyex,float eyey,float eyez){
        pmvMatrix.glMatrixMode(PMVMatrix.GL_MODELVIEW);
        pmvMatrix.glLoadIdentity();
        pmvMatrix.gluPerspective(75,((float)(width))/height,1,7);
        pmvMatrix.gluLookAt(eyex,eyey,eyez,0f,0f,0f,0f,1f,0f);
    }

    @Override
    public void initWindows()
    {
        GLCapabilities caps = new GLCapabilities(GLProfile.get(GLProfile.GL2ES2));
        caps.setBackgroundOpaque(true);
        GLWindow glWindow = GLWindow.create(caps);

        glWindow.setTitle("Photoparty");
        glWindow.setSize(this.width,this.height);
        glWindow.setUndecorated(false);
        glWindow.setPointerVisible(false);
        glWindow.setVisible(true);

        glWindow.addGLEventListener(this);
        Animator animator = new Animator();
        animator.add(glWindow);
        animator.start();
    }

    public void setRenderMode(int renderMode){
        this.configurationChanged = this.renderMode!=renderMode;
        this.renderMode = renderMode;
    }

    @Override
    public void menuMode()
    {
        this.setRenderMode(MENU);
    }

    @Override
    public void diaporamaMode(){
        this.setRenderMode(DIAPORAMA);
        this.request = LOAD_IMAGE;
    }

    @Override
    public void refreshVote(int scoreVote) {
        this.diapo.setVote(scoreVote);
    }

    @Override
    public void preload(Photographie photo) {
        this.photo_preload = photo;
        this.request = PRELOAD_IMAGE;
    }

    @Override
    public void supprimerPhotographie(Photographie photographie) {
        this.diapo.libererPhoto(photographie);
    }
}
