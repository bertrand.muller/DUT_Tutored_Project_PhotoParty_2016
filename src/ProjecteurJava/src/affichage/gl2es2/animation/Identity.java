package affichage.gl2es2.animation;

import com.jogamp.opengl.math.Matrix4;

/**
 * Created by thomas on 29/03/16.
 */
public class Identity extends Transition {

    public static Identity identity = new Identity();

    public Identity() {
        super(100);
        this.m.loadIdentity();
    }

    @Override
    public void updateMatrix() {

    }

    @Override
    public Matrix4 getMatrix(){
        this.m.loadIdentity();
        return this.m;
    }
}
