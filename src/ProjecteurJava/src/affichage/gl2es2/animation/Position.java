package affichage.gl2es2.animation;

import affichage.gl2es2.Constantes;
import com.jogamp.opengl.math.Matrix4;

/**
 * Created by thomas on 29/03/16.
 */
public class Position extends Transition {
    public static Position default_position = new Position(new Vector3D(0,0, Constantes.POSITION_Z));

    private Vector3D position;

    public Position(Vector3D position) {
        super(100);
        this.position = position;
        this.m.loadIdentity();
        this.m.translate(position.x,position.y,position.z);
    }

    @Override
    public void updateMatrix() {

    }

    @Override
    public Matrix4 getMatrix(){
        this.m.loadIdentity();
        this.m.translate(position.x,position.y,position.z);
        return this.m;
    }

    @Override
    public String toString(){
        return position.toString();
    }
}
