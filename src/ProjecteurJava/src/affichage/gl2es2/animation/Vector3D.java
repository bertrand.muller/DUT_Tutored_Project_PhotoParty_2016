package affichage.gl2es2.animation;

/**
 * Created by thomas on 29/03/16.
 */
public class Vector3D {

    public float x;
    public float y;
    public float z;

    public Vector3D(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Vector3D(Vector3D copy){
        this.x = copy.x;
        this.y = copy.y;
        this.z = copy.z;
    }

    public Vector3D(float x,float y,float z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void sub(float dx,float dy,float dz){
        this.x-=dx;
        this.y-=dy;
        this.z-=dz;
    }

    public void sub(Vector3D vector3D){
        this.sub(vector3D.x,vector3D.y,vector3D.z);
    }

    public void add(float dx,float dy,float dz){
        this.x+=dx;
        this.y+=dy;
        this.z+=dz;
    }

    public void add(Vector3D vector3D){
        this.add(vector3D.x,vector3D.y,vector3D.z);
    }


    public void div(float sx,float sy,float sz){
        this.x/=sx;
        this.y/=sy;
        this.z/=sz;
    }

    public void div(Vector3D vector3D){
        this.div(vector3D.x,vector3D.y,vector3D.z);
    }

    public void div(float div){
        this.div(div,div,div);
    }

    public void mul(float mx,float my,float mz){
        this.x/=mx;
        this.y/=my;
        this.z/=mz;
    }

    public void mul(Vector3D vector3D){
        this.mul(vector3D.x,vector3D.y,vector3D.z);
    }

    public void mul(float mul){
        this.mul(mul,mul,mul);
    }

    public String toString(){
        return "Vector3D [ "+x+","+y+","+z+"]";
    }
}
