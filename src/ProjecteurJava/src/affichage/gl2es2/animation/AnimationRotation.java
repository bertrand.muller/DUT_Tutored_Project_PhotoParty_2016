package affichage.gl2es2.animation;


/**
 * Created by thomas on 29/03/16.
 */
public class AnimationRotation extends Animation {

    private Vector3D rotation_axe;
    private float rspeed;
    private float rdep;

    public AnimationRotation(Transition transition, long duration, Vector3D rotation_axe, float rspeed) {
        super(transition,duration);
        this.rotation_axe = rotation_axe;
        this.rspeed = rspeed;
        this.rdep = 0;
    }

    public AnimationRotation(Transition transition, long duration, Vector3D rotation_axe, float rdep, float rend) {
        super(transition,duration);
        this.rotation_axe = rotation_axe;
        this.rspeed = (rend-rdep)/(float)duration;
        this.m.loadIdentity();
        this.rdep = rdep;
        this.m.rotate(rdep,
                rotation_axe.x,
                rotation_axe.y,
                rotation_axe.z
        );
    }

    @Override
    public void start(){
        super.start();
        this.m.rotate(rdep,
                rotation_axe.x,
                rotation_axe.y,
                rotation_axe.z
        );
    }
    @Override
    public void updateMatrix(){
        super.updateMatrix();
        this.m.rotate(
                rspeed,
                rotation_axe.x,
                rotation_axe.y,
                rotation_axe.z
        );
    }
}
