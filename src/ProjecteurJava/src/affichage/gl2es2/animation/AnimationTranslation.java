package affichage.gl2es2.animation;

/**
 * Created by thomas on 29/03/16.
 */
public class AnimationTranslation extends Animation {

    private Vector3D speed;
    private Vector3D basePos;

    public AnimationTranslation(Transition transition, long duration, Vector3D speed) {
        super(transition,duration);
        this.speed = speed;
        this.basePos = new Vector3D(0,0,0);
        this.m.loadIdentity();
        this.m.translate(
                this.basePos.x,
                this.basePos.y,
                this.basePos.z
        );
    }

    public AnimationTranslation(Transition transition, long duration, Vector3D start, Vector3D end){
        super(transition,duration);
        this.speed = new Vector3D(end);
        this.speed.sub(start);
        this.speed.div(duration);
        this.m.loadIdentity();
        this.basePos = start;
        this.m.translate(
                this.basePos.x,
                this.basePos.y,
                this.basePos.z
        );
    }

    @Override
    public void start(){
        super.start();
        this.m.translate(
                basePos.x,
                basePos.y,
                basePos.z
        );
    }

    @Override
    public void updateMatrix() {
        super.updateMatrix();
        this.m.translate(
                this.speed.x,
                this.speed.y,
                this.speed.z
        );
    }
}
