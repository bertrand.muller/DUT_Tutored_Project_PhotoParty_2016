package affichage.gl2es2.animation;

import com.jogamp.opengl.math.Matrix4;

/**
 * Created by thomas on 29/03/16.
 */
public class Animation extends Transition {

    protected Matrix4 etatFinal;
    protected Transition animation;

    public Animation(Transition animation, long duration)
    {
        super(duration);
        this.etatFinal = new Matrix4();
        this.animation = animation;
        this.m.loadIdentity();
    }

    @Override
    public void start(){
        this.etatFinal.loadIdentity();
        super.start();
        this.animation.start();
    }

    @Override
    public void stop(){
        this.etatFinal.loadIdentity();
        super.stop();
        this.animation.stop();
    }

    @Override
    public void updateMatrix() {
        this.animation.updateMatrix();
    }

    @Override
    public Matrix4 getMatrix(){
        this.etatFinal.loadIdentity();
        this.etatFinal.multMatrix(this.m);
        this.etatFinal.multMatrix(animation.getMatrix());
        return this.etatFinal;
    }
}
