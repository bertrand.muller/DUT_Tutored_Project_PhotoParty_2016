package affichage.gl2es2.animation;

import affichage.gl2es2.Constantes;
import com.jogamp.opengl.math.Matrix4;

/**
 * Created by thomas on 29/03/16.
 */
public abstract class Transition {

    public static final int N_TRANSITION = 2;
    public static final int ROTATION_Z_ZOOM=0,ROTATION_Y_ZOOM=1;

    protected Matrix4 m;
    protected long duration;
    protected long start;
    protected boolean isStop;

    public Transition(long duration){
        this.duration = duration;
        this.m = new Matrix4();
        this.start = 0;
        this.isStop = true;
    }

    public boolean isStart(){
        return !this.isStop;
    }

    public void start(){
        this.start = 0;
        this.m.loadIdentity();
        this.isStop = false;
    }

    public void stop(){
        this.isStop = true;
    }

    public void update(){
        if(!isStop){
            if(start < this.duration) {
                this.start++;
                this.updateMatrix();
            }else
                isStop = true;
        }
    }

    public abstract void updateMatrix();

    public Matrix4 getMatrix(){
        return this.m;
    }

    public long getTime(){
        return this.start;
    }



    private static Transition preprogrammed[] = new Transition[]
    {
        new AnimationTranslation(
                new AnimationRotation(
                        new Identity(),
                        100,
                        new Vector3D(0f,0f,1f),0f,(float)Math.toRadians(360)
                )
                ,100,
                new Vector3D(0,0,-5f),new Vector3D(0,0,Constantes.POSITION_Z)
        ),
        new AnimationTranslation(
                    new AnimationRotation(
                            new Identity(),
                            100,
                            new Vector3D(0f,1f,0f),0f,(float)Math.toRadians(360)
                    )
                    ,100,
                    new Vector3D(0,0,-5f),new Vector3D(0,0,Constantes.POSITION_Z)
            )
    };

    public static Transition getTransition(int id){
        if(id >= 0 && id < N_TRANSITION)
            return preprogrammed[id];
        else
            return getDefaultTransition();
    }

    public static Transition getDefaultTransition(){
        return Position.default_position;
    }
}
