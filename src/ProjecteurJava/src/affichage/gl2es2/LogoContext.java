package affichage.gl2es2;

import affichage.gl2es2.shapes.TexturedCube;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.util.PMVMatrix;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.Buffer;

/**
 * Created by thomas on 04/03/16.
 */
public class LogoContext extends GL2ES2Context {

    private Matrix4 rotateMatrix;

    private TexturedCube cube;


    public LogoContext(){
        this.rotateMatrix = new Matrix4();
        this.rotateMatrix.loadIdentity();
    }

    @Override
    public void init(GL2ES2 gl){
        super.init(gl);
        this.cube = new TexturedCube(this);
        this.cube.init(gl);
    }

    protected void initTexture(GL2ES2 gl){
        super.initTexture(gl);
        try {
            //FileInputStream f = new FileInputStream("Yoda.jpg");
            //TextureData tx_data = TextureIO.newTextureData(gl.getGLProfile(),f,false,TextureIO.JPG);
            BufferedImage img = ImageIO.read(new File("res/logo.png"));
            BufferedImage imgnew = new BufferedImage(img.getWidth(),img.getHeight(),BufferedImage.TYPE_4BYTE_ABGR);
            Graphics2D g2d = imgnew.createGraphics();
            g2d.drawImage(img,0,0,null);
            g2d.dispose();
            TextureData tx_data = AWTTextureIO.newTextureData(gl.getGLProfile(),imgnew,false);
            System.out.println("Texture width : "+tx_data.getWidth());
            System.out.println("Texture height : "+tx_data.getHeight());

            Buffer pix_buffer = tx_data.getBuffer();


            gl.glActiveTexture(GL2ES2.GL_TEXTURE0); // select active texture unit
            gl.glBindTexture(GL2ES2.GL_TEXTURE_2D,texturenames[0]); // bind texturenames[0] with the active texture unit 0

            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR);
            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR);

            gl.glTexImage2D(
                    GL2ES2.GL_TEXTURE_2D,0,
                    GL2ES2.GL_RGBA,tx_data.getWidth(),tx_data.getHeight(),
                    0, GL2ES2.GL_RGBA, GL2ES2.GL_UNSIGNED_BYTE,pix_buffer);
            imgnew = null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void render(GL2ES2 gl, PMVMatrix pmv,double delta){
        gl.glUseProgram(shaderProgram);
        this.cube.update(pmv,delta);
        this.cube.render(gl);
    }

    @Override
    public int VBOCount() {
        return 2;
    }

    @Override
    public int TextureCount() {
        return 1;
    }
}
