package affichage.gl2es2;

import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.util.PMVMatrix;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.IOException;

/**
 * Classe pour gérer une forme texturée OpenGL2ES2
 */
public abstract class GL2ES2Context {
    /*
     * Chemin vers les fichiers source pour le GPU
     */
    private static final String VSH_PATH="src/affichage/gl2es2/shadersSource/vertexTexturedShader.vsh";
    private static final String FSH_PATH="src/affichage/gl2es2/shadersSource/fragmentTexturedShader.fsh";

    private boolean isInit;
    /*
     * Lien vers les variables dans le GPU
     *  ATTRIBUTE_POSITION -> attribute_Position (coordonnées vertices)
     *  ATTRIBUTE_TEXCOORD -> a_texCoord (coordonnées texture)
     */
    public static final int ATTRIBUTE_POSITION=0,ATTRIBUTE_TEXCOORD=1;
    //Indices dans le tableau VBO (Vertex Buffer Object)
    public static final int VERTICES_IDX = 0,TEXCOORD_IDX = 1;

    /* Matrice resultante de toute les transformations */
    protected float [] resultMatrix;
    /* Tableau des VBO */
    protected int[] vboHandles;
    /* Tableau des nom de texture */
    protected int[] texturenames;

    /* Lien vers le bout de programme s'occupant des vertices */
    protected int vertexShader;
    /* Lien vers le bout de programme s'occupant de la texture */
    protected int fragShader;
    /* Lien vers le programme GPU compile */
    protected int shaderProgram;
    /*
     * Lien vers la variable matrice finale MVP (Model View Projection)
     * uniform_Projection
     */
    protected int mHandleMVPMatrix;
    /*
     * Lien vers la variable recevant la texture
     * s_texture
     */
    protected int mHandleTexture;

    public GL2ES2Context(){
        this.resultMatrix = new float[16];
    }

    public void init(GL2ES2 gl){
        this.isInit = true;
        this.initShaderProgramm(gl);
        this.initTexture(gl);
    }

    /**
     * Compile les sources fragmentTexturedShader.fsh et vertexTexturedShader.vsh
     * Et lie les variables
     *   attribute_Position,a_texCoord
     *   uniform_Projection,s_texture
     * @param gl
     *  contexte
     */
    protected void initShaderProgramm(GL2ES2 gl)
    {
        try {
            this.vertexShader = GL2ES2Context.initShaders(gl, GL2ES2.GL_VERTEX_SHADER, new File(VSH_PATH));
            this.fragShader = GL2ES2Context.initShaders(gl, GL2ES2.GL_FRAGMENT_SHADER, new File(FSH_PATH));
        }catch(IOException e){
            System.err.println("Shader code can't be loaded : "+e.getMessage());
            System.exit(0);
        }
        this.shaderProgram = gl.glCreateProgram();

        gl.glAttachShader(shaderProgram,vertexShader);
        gl.glAttachShader(shaderProgram,fragShader);

        gl.glBindAttribLocation(shaderProgram,ATTRIBUTE_POSITION,"attribute_Position");
        gl.glBindAttribLocation(shaderProgram,ATTRIBUTE_TEXCOORD,"a_texCoord");

        gl.glLinkProgram(shaderProgram);

        this.mHandleMVPMatrix = gl.glGetUniformLocation(shaderProgram,"uniform_Projection");
        this.mHandleTexture = gl.glGetUniformLocation(shaderProgram,"s_texture");

        this.initVBO(gl);
    }

    /**
     * Initialise les textures
     * @param gl
     *  contexte
     */
    protected void initTexture(GL2ES2 gl){
        this.initTextureNames(gl);
    }

    /**
     * Libère le programme du GPU
     * @param gl
     *  contexte
     */
    private void releaseShaderProgram(GL2ES2 gl){
        gl.glDetachShader(shaderProgram, vertexShader);
        gl.glDeleteShader(vertexShader);
        gl.glDetachShader(shaderProgram, fragShader);
        gl.glDeleteShader(fragShader);
        gl.glDeleteProgram(shaderProgram);
    }

    /**
     * Initialise les VBO
     * @param gl
     *  contexte
     */
    private void initVBO(GL2ES2 gl)
    {
        this.vboHandles = new int[VBOCount()];
        gl.glGenBuffers(VBOCount(),this.vboHandles,0);
    }

    /**
     * Libère les VBO
     * @param gl
     *  contexte
     */
    private void releaseVBO(GL2ES2 gl)
    {
        gl.glDeleteBuffers(VBOCount(),this.vboHandles,0);
        this.vboHandles = null;
    }

    /**
     * Initialise les nom de textures
     * @param gl
     *  contexte
     */
    private void initTextureNames(GL2ES2 gl){
        this.texturenames = new int[TextureCount()];
        gl.glGenTextures(TextureCount(),this.texturenames,0);
    }

    /**
     * Libère les nom de textures
     * @param gl
     */
    private void releaseTexture(GL2ES2 gl){
        gl.glDeleteTextures(TextureCount(),this.texturenames,0);
        this.texturenames = null;
    }

    /**
     * Libère la forme
     * @param gl
     */
    public void release(GL2ES2 gl)
    {
        gl.glUseProgram(0);
        this.releaseVBO(gl);
        this.releaseTexture(gl);
        this.releaseShaderProgram(gl);
        this.isInit = false;
    }

    /**
     * Compile une source pour le GPU
     * @param gl
     *  contexte
     * @param type
     *  GL_VERTEX_SHADER ou GL_FRAGMENT_SHADER
     * @param shaderSource
     *  code source
     * @return
     *  Lien vers la source compilé
     */
    public static final int initShaders(GL2ES2 gl, int type, String shaderSource){
        int shader = gl.glCreateShader(type);
        int[] compiled = new int[1];
        //Compile the Shader String into a program.
        String[] flines = new String[] { shaderSource };
        int[] flengths = new int[] { flines[0].length() };
        gl.glShaderSource(shader, flines.length, flines, flengths, 0);
        gl.glCompileShader(shader);

        //Check compile status.
        gl.glGetShaderiv(shader, GL2ES2.GL_COMPILE_STATUS, compiled,0);
        if(compiled[0]!=0){System.out.println("Horray! fragment shader compiled");}
        else {
            int[] logLength = new int[1];
            gl.glGetShaderiv(shader, GL2ES2.GL_INFO_LOG_LENGTH, logLength, 0);

            byte[] log = new byte[logLength[0]];
            gl.glGetShaderInfoLog(shader, logLength[0], (int[])null, 0, log, 0);

            System.err.println("Error compiling the fragment shader: " + new String(log));
            System.exit(1);
        }
        return shader;
    }

    /**
     * Compile une source pour le GPU
     * @param gl
     *  contexte
     * @param type
     *  GL_VERTEX_SHADER ou GL_FRAGMENT_SHADER
     * @param shaderFileSource
     *  fichier source
     * @return
     *  Lien vers la source compilé
     * @throws IOException
     */
    public static final int initShaders(GL2ES2 gl, int type, File shaderFileSource) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(shaderFileSource));
        String source = "";
        String line = reader.readLine();
        while(line != null) {
            source += line + "\n";
            line = reader.readLine();
        }
        return initShaders(gl,type,source);
    }

    public boolean isInit(){
        return this.isInit;
    }

    /**
     * Méthode de rendu , dessine la forme sur l'ecran
     * @param gl
     *  contexte
     * @param pmv
     *  Projection Model View Matrice
     * @param delta
     *  temps ecoulé
     */
    public abstract void render(GL2ES2 gl, PMVMatrix pmv, double delta);
    public abstract int VBOCount();
    public abstract int TextureCount();

    public int [] getVboHandles(){
        return this.vboHandles;
    }

    public int [] getTexturenames(){
        return this.texturenames;
    }

    public int getmHandleTexture(){
        return this.mHandleTexture;
    }

    public int getmHandleMVPMatrix(){
        return this.mHandleMVPMatrix;
    }
}
