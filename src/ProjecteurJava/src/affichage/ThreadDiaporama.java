package affichage;

import modeles.Configuration;

/**
 * Created by thomas on 26/01/16.
 */
public class ThreadDiaporama extends Thread{

    private static ThreadDiaporama threadDiaporama;
    private static Galerie galerie;

    /**
     * Associe le ThreadDiaporama a la galerie g
     * @param g
     */
    public static void setup(Galerie g){
        galerie = g;
    }

    public static synchronized ThreadDiaporama getInstance(){
        if(threadDiaporama == null)threadDiaporama = new ThreadDiaporama();
        return threadDiaporama;
    }

    /**
     * Fait défiler les images en boucle
     */
    @Override
    public void run(){
        while(true){
            try {
                if(!Configuration.getInstance().isStop())
                    galerie.suivante();
                Thread.sleep(Configuration.getInstance().getTransitionTime());
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
