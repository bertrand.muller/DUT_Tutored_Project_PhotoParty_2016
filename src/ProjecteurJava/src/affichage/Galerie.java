package affichage;


import modeles.Photographie;

import java.util.ArrayList;
import java.util.Observable;

public class Galerie extends Observable {

	private int index;
	private Projecteur projecteur;
	private ArrayList<Photographie>	liste = new ArrayList<Photographie>();

	public Galerie(Projecteur projecteur)
	{
		this.index = 0;
		this.projecteur = projecteur;
		this.projecteur.attachGalerie(this);
	}

	public Projecteur getProjecteur(){
		return this.projecteur;
	}

	public void ajouterPhotographie(Photographie photo){
		liste.add(photo);
		this.projecteur.preload(photo);
	}

	public void supprimerPhotographie(int id){
        System.out.println("[GALERIE] Suppression de la photo "+id);
        for(int i = 0; i < liste.size(); i++) {
            if(liste.get(i).getId() == id){
				this.projecteur.supprimerPhotographie(liste.get(i));
                liste.remove(i);
                if(this.index >= liste.size())
                    this.index = 0;
                break;
            }
        }
		this.projecteur.updatePhotographie();
    }
	/**
	 * Renvoie la photographie courrante
	 * @return
     */
	public Photographie photographieCourante(){
        if(!liste.isEmpty()) {
            Photographie photographie = liste.get(index);
            return photographie;
        }else
            return null;
	}

    /**
     * Rafraichi l'ecran
     */
	public void refresh(){
		this.projecteur.refreshVote((int)liste.get(index).scoreVote());
	}

    /**
     * Passe a la photo suivante ou retourne au début du diaporama
     */
	public synchronized void suivante(){
		System.out.println("[GALERIE] Image suivante");
		this.index++;
		if(this.index >= this.liste.size())
			this.index = 0;
		this.projecteur.updatePhotographie();
	}

	public ArrayList<Photographie> getListe(){
		return this.liste;
	}

	public int getIndex(){
		return this.index;
	}
}

