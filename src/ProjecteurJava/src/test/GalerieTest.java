package test;

import affichage.Galerie;
import modeles.Photographie;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by thomas on 23/01/16.
 */
public class GalerieTest {

    private Galerie galerie;
    @Before
    public void setUp() throws Exception {
        this.galerie = new Galerie(null);
    }

    @Test
    public void testAjouterPhotographie() throws Exception {
        this.galerie.ajouterPhotographie(new Photographie("imageTest/1920x1080.jpeg"));
        assertEquals("La photo n'a pas été ajoutée",this.galerie.getListe().size(),1);
    }

    @Test
    public void testPhotographieCourante() throws Exception {
        Photographie p1 = new Photographie("blabla");
        Photographie p2 = new Photographie("imageTest/1920x1080.jpeg");
        this.galerie.ajouterPhotographie(p1);
        this.galerie.ajouterPhotographie(p2);
        assertEquals("Mauvaise photo",p2,this.galerie.photographieCourante());
        assertNotNull("La photo n'a pas été chargée",this.galerie.photographieCourante());
    }
}