package test;

import affichage.Galerie;
import affichage.Projecteur;
import modeles.Photographie;
import affichage.swing.ProjecteurSwing;

/**
 * Created by thomas on 23/01/16.
 */
public class MainTest {

    public static void main(String[] args){
        Projecteur projecteurSwing = new ProjecteurSwing();
        Galerie galerie = new Galerie(projecteurSwing);
        projecteurSwing.initWindows();
        galerie.ajouterPhotographie(new Photographie("imageTest/1920x1080.jpeg"));
    }

}
