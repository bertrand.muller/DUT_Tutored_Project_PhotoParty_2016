package test;

import modeles.Photographie;

import static org.junit.Assert.*;

/**
 * Created by thomas on 23/01/16.
 */
public class PhotographieTest {

    private Photographie photographie;

    @org.junit.Before
    public void constructImage(){
        this.photographie = new Photographie("imageTest/1920x1080.jpeg");
    }

    @org.junit.Test
    public void testConstructeur() throws Exception{
        assertEquals("Mauvais chemin",this.photographie.getChemin(),"imageTest/1920x1080.jpeg");
        assertNull("L'image ne doit pas être chargée",this.photographie.getImage());
    }

    @org.junit.Test
    public void testChargerImage() throws Exception {
        this.photographie.chargerImage();
        assertNotNull("L'image n'est pas chargée",this.photographie.getImage());
    }

    @org.junit.Test
    public void testDechargerImage() throws Exception {
        this.photographie.chargerImage();
        assertNotNull("L'image n'est pas chargée",this.photographie.getImage());
        this.photographie.dechargerImage();
        assertNull("L'image n'est pas déchargée",this.photographie.getImage());
    }
}