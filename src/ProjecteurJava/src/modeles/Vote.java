package modeles;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by thomas on 12/02/16.
 * Represente un tuple de la relation Vote
 */
public class Vote {

    private static final String TABLE="Vote";

    private int id_photo;
    private int ip_user;
    private byte avis;


    private Vote(int id_photo,int ip_user,byte avis){
        this.id_photo = id_photo;
        this.ip_user = ip_user;
        this.avis = avis;
    }

    /**
     * Récupere la liste des votes associé a l'id_photo
     * @param id_photo
     * @return
     */
    public static ArrayList<Vote> findAll(int id_photo){
        ArrayList<Vote> votes = new ArrayList<Vote>();
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT ip,avis FROM `"+TABLE+"` WHERE photo_id=?");
            statement.setInt(1,id_photo);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                int ip = resultSet.getInt(1);
                byte avis = 0;
                switch (resultSet.getCharacterStream(2).read()){
                    case 'n':
                            avis = 0;
                        break;
                    case 's':
                            avis = 1;
                        break;
                    case 'c':
                            avis = 2;
                        break;
                    case 'g':
                            avis = 3;
                        break;
                }

                votes.add(new Vote(id_photo,ip,avis));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return votes;
    }

    public int getScore(){
        return this.avis;
    }

    public int getIpUser(){
        return this.ip_user;
    }
}
