package modeles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by thomas on 25/01/16.
 * Représente un tuple de la relation Stockage
 */
public class Stockage {

    private static final String TABLE="Stockage";
    private int id;
    private String prefix;
    private String type;

    public Stockage(){
        this.id = -1;
    }

    /**
     * Récupere un stockage via son id
     * @param primaryKey
     * @return
     */
    public static Stockage find(int primaryKey){
        Stockage stockage = null;
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT prefix,type FROM `"+TABLE+"` WHERE id=?");
            statement.setInt(1,primaryKey);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
            {
                stockage = new Stockage();
                stockage.type = resultSet.getString(2);
                stockage.prefix = resultSet.getString(1);
                stockage.id = primaryKey;
                resultSet.close();
            }
            statement.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return stockage;
    }

    public String getPrefix(){
        return this.prefix;
    }

}
