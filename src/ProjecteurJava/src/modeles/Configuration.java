package modeles;

import affichage.ThreadDiaporama;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by thomas on 14/02/16.
 * Classe contenant la configuration de l'application
 */
public class Configuration {

    public static final int ADMIN_PSEUDO=1,ADMIN_PASSWORD=2,STOCKAGE_SELECT=3,TRANSITION_TIME=4,DIAPORAMA_STATE=5,ETHERNET_STATE=6,TRANSITION_MASK=7;

    private static final String TABLE_NAME="Configuration";
    private static Configuration config;

    private String pseudo;
    private String passwordhash;
    private int selected_stockage;
    private long transition_time;
    private int transition_mask;
    private boolean stop;
    private boolean ethActif;

    private Configuration(){
        this.load();
    }

    /**
     * Singleton (1 seule configuration)
     * @return configuration
     */
    public static synchronized Configuration getInstance(){
        if(config == null)config = new Configuration();
        return config;
    }

    /**
     * Charge la configuration depuis la base de données
     */
    public void load(){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id,config_value FROM `"+TABLE_NAME+"` ORDER BY id");

            if(resultSet.next()) this.pseudo = resultSet.getString(2);
            if(resultSet.next()) this.passwordhash = resultSet.getString(2);
            if(resultSet.next()) this.selected_stockage = Integer.parseInt(resultSet.getString(2));
            if(resultSet.next()) this.transition_time = Long.parseLong(resultSet.getString(2));

            if(resultSet.next()) {
                if (resultSet.getString(2).equals("1")){
                    this.stop = false;
                }else {
                    this.stop = true;
                }
            }

            if(resultSet.next()){
                if(resultSet.getString(2).equals("1"))
                    this.ethActif = true;
                else
                    this.ethActif = false;
            }

            if(resultSet.next())this.transition_mask = Integer.parseInt(resultSet.getString(2));

            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized long getTransitionTime(){
        return this.transition_time;
    }

    public synchronized int getTransitionMask() {
        return this.transition_mask;
    }

    public synchronized boolean isStop(){
        return this.stop;
    }

    public synchronized boolean isEthActif(){
        return this.ethActif;
    }

}
