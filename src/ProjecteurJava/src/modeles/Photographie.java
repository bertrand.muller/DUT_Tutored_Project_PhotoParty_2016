package modeles;



import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

import javax.imageio.ImageIO;

/**
 * Représente un tuple de la relation Photographie
 */
public class Photographie {
	private static final String TABLE="Photographie";
	private int width;
	private int height;
	private int id;
	private int id_stockage;
	private int ip_user;
	private byte hash [];
	private String path;
	private String chemin;
	private BufferedImage image;

    public Photographie(){
        this.id = -1;
    }

	public Photographie(String chemin){
		this.id=-1;
		this.chemin=chemin;
	}

    /**
     * Charge l'image en RAM
     */
	public void chargerImage(){
		try {
			if(this.image == null) {
				System.out.println("[IMAGE] Acces disque : " + chemin);
				this.image = ImageIO.read(new File(chemin));
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

    /**
     * Met l'image a null
     * (l'image seras décharger de la memoire lors de l'appel du garbage collector)
     */
	public void dechargerImage(){
		this.image.flush();
		this.image = null;
	}

	public BufferedImage getImage() {
		return image;
	}

    public String getChemin(){
        return this.chemin;
    }

    /**
     * Récupere une photographie par son id
     * @param primaryKey
     * @return Photographie
     */
	public static Photographie find(int primaryKey){
		Photographie photographie = null;
		Connection connection = DBConnection.getInstance().getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT id_stockage,ip_user,hash,path,width,height FROM `"+TABLE+"` WHERE id=?");
			statement.setInt(1,primaryKey);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()){
                photographie = new Photographie();
				photographie.id_stockage = resultSet.getInt(1);
				photographie.ip_user = resultSet.getInt(2);
				photographie.hash = resultSet.getBytes(3);
				photographie.path = resultSet.getString(4);
				photographie.width = resultSet.getInt(5);
				photographie.height = resultSet.getInt(6);
				photographie.id = primaryKey;
				photographie.chemin = photographie.getStockage().getPrefix()+File.separatorChar+photographie.path;
				resultSet.close();
			}
            statement.close();
        }catch (Exception e){
			e.printStackTrace();
		}
		return photographie;
	}

    /**
     * Retourne le stockage associé a la photographie
     * @return Stockage
     */
	public Stockage getStockage(){
		return Stockage.find(this.id_stockage);
	}

	public int getId(){
		return this.id;
	}

    /**
     * Calcul le score de la photo
     * @return le score de la photo
     */
	public double scoreVote()
    {
        ArrayList<Vote> votes = Vote.findAll(this.id);
        double score = 0;
		if(votes.size()>0) {
			for (Vote v : votes)
				score += v.getScore();
			score /= (votes.size());
		}
        return score;
    }

	public int getWidth(){
		return width;
	}

	public int getHeight(){
		return height;
	}
}
