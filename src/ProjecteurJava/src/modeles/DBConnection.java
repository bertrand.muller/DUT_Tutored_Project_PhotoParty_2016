package modeles;

import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by thomas on 25/01/16.
 *
 */
public class DBConnection {

    private static DBConnection conn;
    private Connection connection;

    public DBConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        SQLiteConfig config = new SQLiteConfig();
        config.setReadOnly(true);
        this.connection = DriverManager.getConnection("jdbc:sqlite:/var/www/ProjetTutore/bdd.sqlite",config.toProperties());
        this.connection.setAutoCommit(true);
    }

    /**
     * Fourni une seule connexion en LECTURE SEULE
     * @return Connexion a la base de données
     */
    public static synchronized DBConnection getInstance(){
        try {
            if (conn == null)
                conn = new DBConnection();
            else if(conn.isClosed())
                conn = new DBConnection();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * Ferme la connection
     */
    public void close(){
        try {
            this.connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Indique si la connexion est fermée
     * @return vrai si la connexion est fermée
     */
    public boolean isClosed(){
        try {
            return this.connection.isClosed();
        }catch(Exception e){

        }
        return true;
    }

    public Connection getConnection(){
        return this.connection;
    }
}
