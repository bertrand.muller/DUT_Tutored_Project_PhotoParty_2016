import java.io.*;

import affichage.Projecteur;
import affichage.gl2es2.ProjecteurGL2ES2;
import affichage.ThreadDiaporama;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.Animator;
import serveur.ServeurAffichage;
import affichage.Galerie;
import affichage.swing.ProjecteurSwing;


public class Main {

	public static void main(String [] args) throws IOException{
		Projecteur projecteur = new ProjecteurGL2ES2();
		Galerie galerie = new Galerie(projecteur);
		projecteur.initWindows();
		ServeurAffichage server = new ServeurAffichage(galerie);
		server.start();
		ThreadDiaporama.setup(galerie);
		ThreadDiaporama.getInstance().start();
	}
}
