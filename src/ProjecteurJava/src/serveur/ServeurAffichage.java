package serveur;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import affichage.Galerie;
import modeles.Photographie;
import org.newsclub.net.unix.AFUNIXServerSocket;
import org.newsclub.net.unix.AFUNIXSocketAddress;

/**
 * Classe pour gérer des connexions simultanée au serveur diaporama
 * Le serveur écoute sur le port 1337
 * @author thomas
 */
public class ServeurAffichage extends Thread{

	private Galerie galerie;
	private AFUNIXServerSocket listener;
	/* Liste des clients connectés */
	private List<JavaCommunication> clients;
	
	public ServeurAffichage(Galerie galerie) throws IOException{
		this.listener = AFUNIXServerSocket.newInstance();
		this.listener.bind(new AFUNIXSocketAddress(new File("/tmp/diaporama_unix_socket")));
		this.clients = new ArrayList<JavaCommunication>();
		this.galerie = galerie;
	}
	
	public void removeCom(JavaCommunication com){
		this.clients.remove(com);
	}
	
	public synchronized void transfererPhoto(Photographie photographie){
		this.galerie.ajouterPhotographie(photographie);
	}

	public synchronized void supprimerPhoto(int index) {
		this.galerie.supprimerPhotographie(index);
	}
	/**
	 * Attend les clients sur le port 1337
	 */
	@Override
	public void run(){
		while(true){
			Socket sock;
			try {
				sock = this.listener.accept();
				JavaCommunication com = new JavaCommunication(this,sock);
				this.clients.add(com);
				com.start();
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}

	public Galerie getGalerie(){
		return this.galerie;
	}
}
