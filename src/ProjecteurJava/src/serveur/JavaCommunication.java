package serveur;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import affichage.ThreadDiaporama;
import modeles.Configuration;
import modeles.Photographie;

/**
 * Classe pour gérer la communication avec une seule session PHP
 * @author thomas
 */
public class JavaCommunication extends Thread{

	/**
	 * Commande de contrôle : Utilisateur simple
	 *  PHOTO_UPLOAD : une photo a été reçu coté PHP
	 *  QUERY_CURRENT : demande l'id de la photo courrante
     *  REFRESH_SCREEN : rafraichissement de l'ecran
	 *  CONFIG_CHANGE : reload la configuration
     *  SUPPR_PHOTO : supprime la photographie
	 */
	private static final byte PHOTO_UPLOAD=1;
	private static final byte QUERY_CURRENT=2;
	private static final byte REFRESH_SCREEN=3;
    private static final byte CONFIG_CHANGE=4;
	private static final byte SUPPR_PHOTO=5;
	private static final byte GET_WIDTH=6;
	private static final byte GET_HEIGHT=7;

	private DataInputStream in;
	private DataOutputStream out;
	private ServeurAffichage server;
	
	/**
	 * Initialise les flux de communication a partir de l'objet sock
	 * @param server
	 * 	server d'affichage parent
	 * @param sock
	 *  source
	 * @throws IOException
	 */
	public JavaCommunication(ServeurAffichage server,Socket sock) throws IOException{
		this.server = server;
		this.in  = new DataInputStream(sock.getInputStream());
		this.out = new DataOutputStream(sock.getOutputStream());
	}
	
	/**
	 * Construit un objet photographie (sans charger la photo)
	 * et informe le serveur d'affichage
	 * @throws IOException
	 */
	public void recevoirPhoto() throws IOException{
		int index = this.in.readInt();
		Photographie photo = Photographie.find(index);
		this.server.transfererPhoto(photo);
	}

    /**
     * Supprime la photo avec l'id correspondant
     * @throws IOException
     */
    public void supprimerPhoto() throws IOException{
        int index = this.in.readInt();
        this.server.supprimerPhoto(index);
    }
    /**
     * Recupere l'id de la photo afficher et renvoie son id au serveur PHP
     */
    public void envoyerIdPhotoCourrante() throws IOException
    {
        Photographie photocourrante = this.server.getGalerie().photographieCourante();
        if(photocourrante != null) {
            this.out.writeInt(photocourrante.getId());
        }else
            this.out.writeInt(-1);
        this.out.flush();
    }

	/**
	 * Renvoie la largeur de l'ecran
	 */
	public void renvoyerLargeur(){
		try {
			this.out.writeInt(server.getGalerie().getProjecteur().getWidth());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Renvoie la hauteur de l'ecran
	 */
	public void renvoyerHauteur(){
		try {
			this.out.writeInt(server.getGalerie().getProjecteur().getHeight());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    /**
     * Rafraichi l'ecran
     */
    public void refreshScreen(){
        this.server.getGalerie().refresh();
    }

    /**
     * Charge la nouvelle configuration
     */
    public void loadnewConfig(){
        Configuration config = Configuration.getInstance();
        config.load();
    }
	/**
	 * Ferme les flux de communications
	 * et met a jour la liste des clients connectées
	 */
	public void close(){
		try{
			this.in.close();
			this.out.close();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			this.server.removeCom(this);
		}
	}
	
	/**
	 * Attend l'octets de commande et déclenche l'operation en conséquence
	 */
	@Override
	public void run(){
		try{
			byte command = this.in.readByte();
			switch(command){
				case PHOTO_UPLOAD:
					this.recevoirPhoto();
				break;
                case QUERY_CURRENT:
                    this.envoyerIdPhotoCourrante();
                    break;
                case REFRESH_SCREEN:
                    this.refreshScreen();
                    break;
                case CONFIG_CHANGE:
                    this.loadnewConfig();
                    break;
                case SUPPR_PHOTO:
                    this.supprimerPhoto();
                    break;
				case GET_WIDTH:
					this.renvoyerLargeur();
					break;
				case GET_HEIGHT:
					this.renvoyerHauteur();
					break;
			}
		}catch(IOException e){
			this.close();
		}
		this.close();
	}
}
