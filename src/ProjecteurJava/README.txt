imageTest:
    image pour les tests
lib:
    junixsocket-1.3.jar : bibliothèque pour utiliser les sockets UNIX (fonctionne uniquement sur les systèmes UNIX)
    sqlite-jdbc-3.8.11.2.jar : bibliothèque pour se connecter à une base sqlite via JDBC
lib-native:
    libjunixsocket-linux-1.5-amd64.so : wrapper java pour junixsocket-1.3.jar à recompiler sur arm
src/test:
    dossier de test (junit4)
doc:
    documentation (diagramme de classes ...)

Pour lancer l'application il est nécessaire de spécifier l'emplacement des bibliothèques natives avec l'options
-Djava.library.path=lib-native
