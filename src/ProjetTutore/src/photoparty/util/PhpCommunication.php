<?php
/**
 * Created by PhpStorm.
 * User: MathieuBourgault
 * Date: 06/01/2016
 * Time: 13:05
 */

namespace photoparty\util;


class PhpCommunication
{
    const PHOTO_UPLOAD=1,QUERY_CURRENT=2,REFRESH_SCREEN=3,CONFIG_CHANGE=4,SUPPR_PHOTO=5,GET_WIDTH=6,GET_HEIGHT=7;
    const PIPE_NAME="/tmp/diaporama_unix_socket";
    private static $instance;

    private $descripteur;

    public static function getInstance()
    {
        if(static::$instance == null){
            static::$instance = new PhpCommunication();
        }
        return static::$instance;
    }


    private function sendCommand($id,$params = null,$recv = 0)
    {
        $this->descripteur = socket_create(AF_UNIX,SOCK_STREAM,0);
        socket_connect($this->descripteur,static::PIPE_NAME);
        $buffer = chr($id);
        if($params!=null)
            $buffer.=$params;
        socket_write($this->descripteur,$buffer);
        $ret = '';
        if($recv > 0)
            socket_recv($this->descripteur,$ret,$recv,MSG_WAITALL);
        socket_close($this->descripteur);
        return $ret;
    }

    public function informerProjecteur($id)
    {
        $binary = chr(($id >> 24) & 0xFF).chr(($id >> 16) & 0xFF).chr(($id >> 8) & 0xFF).chr($id & 0xFF);
        $this->sendCommand(PhpCommunication::PHOTO_UPLOAD,$binary);
    }

    public function supprimerPhoto($id)
    {
        $binary = chr(($id >> 24) & 0xFF).chr(($id >> 16) & 0xFF).chr(($id >> 8) & 0xFF).chr($id & 0xFF);
        $this->sendCommand(PhpCommunication::SUPPR_PHOTO,$binary);
    }

    public function getCurrentPhoto(){
        $bytes = $this->sendCommand(PhpCommunication::QUERY_CURRENT,null,4);
        /*$value = 0;
        for($i = 0; $i < strlen($bytes); $i++)
            $value = $value | (ord($bytes[strlen($bytes) - $i - 1])<< ($i * 8));
        return $value;*/
        return self::bytesToInteger($bytes);
    }

    public function getScreenWidth(){
        $bytes = $this->sendCommand(PhpCommunication::GET_WIDTH,null,4);
        return self::bytesToInteger($bytes);
    }

    public function getScreenHeight(){
        $bytes = $this->sendCommand(PhpCommunication::GET_HEIGHT,null,4);
        return self::bytesToInteger($bytes);
    }

    public function configHasChanged(){
        $this->sendCommand(PhpCommunication::CONFIG_CHANGE);
    }

    public function refreshScreen(){
        $this->sendCommand(PhpCommunication::REFRESH_SCREEN);
    }

    private static function bytesToInteger($bytes){
        $value = 0;
        for($i = 0; $i < strlen($bytes); $i++)
            $value = $value | (ord($bytes[strlen($bytes) - $i - 1])<< ($i * 8));
        return $value;
    }
}