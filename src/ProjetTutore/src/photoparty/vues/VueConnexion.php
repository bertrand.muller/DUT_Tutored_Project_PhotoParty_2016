<?php

namespace photoparty\vues;

class VueConnexion extends AbstractVue
{
    private $admin;

    public function __construct($json = false,$admin = false)
    {
        parent::__construct($json);
        $this->admin = $admin;
    }

    public function render($options = null) {
        include('web/tpl/formulaire.html');
    }

    protected function renderJSON($options = null)
    {
        echo json_encode($_SESSION['flash']);
    }
}