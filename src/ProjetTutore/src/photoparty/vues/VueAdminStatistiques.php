<?php

namespace photoparty\vues;

use photoparty\modeles\Photographie;

class VueAdminStatistiques extends AbstractVue
{

    private $photos;

    public function __construct($ph) {
        $this->photos = $ph;
    }

    public function render($options = null) {

        $nbPhotos = count($this->photos);
        $diagrammePlusDeVotes = $this->obtenirDiagrammePlusDeVotes($nbPhotos);
        $diagrammeMeilleursVotes = $this->obtenirDiagrammeMeilleursVotes($nbPhotos);

        $html = <<<END
                <a role="button" data-role="button" id="sidelinkStatistiques" class="sidelink col-xs-1 ui-link ui-btn ui-shadow ui-corner-all" href="adminreg">
                    <span>Réglages</span>
                </a>

                <br><br><br>

                <div class="col-sm-6 col-xs-10 col-center-block blocAdmin">
                   <div class="panel panel-default">
                       <div id="pb" class="panel-body">
                           <span class="glyphicon glyphicon-cog iconAdmin"></span>
                           <h1 class="titreAdmin">Panneau d'Administration</h1>
                           <h3 class="sousTitreAdmin">Statistiques à propos du Diaporama</h3>
                           <br><br><br><br><br>

                            <div class="panel panel-default panel-bordure">
                               <div class="panel-body">
                                    <span class="glyphicon glyphicon-log-in"></span>
                                   <h3>Nombre de photos postées</h3>
                                   <br><br>
                                   <div class="row">
                                       <div class="divCentr">
                                           <p id="nbPhotos">$nbPhotos</p>
                                       </div>
                                   </div>
                               </div>
                            </div>

                            <br><br>

                            <div class="panel panel-default panel-bordure">
                               <div class="panel-body">
                                   <span class="glyphicon glyphicon-align-left"></span>
                                   <h3>Top 5 des photos qui ont<br>obtenu le plus de votes</h3>
                                   <br><br>
                                   <div class="row" style="text-align: center;">
                                       $diagrammePlusDeVotes
                                   </div>
                               </div>
                            </div>

                            <br><br>

                            <div class="panel panel-default panel-bordure">
                               <div class="panel-body">
                                   <span class="glyphicon glyphicon-align-left"></span>
                                   <h3>Top 5 des photos qui ont<br>obtenu les meilleurs votes</h3>
                                   <br><br>
                                   <div class="row" style="text-align: center;">
                                       $diagrammeMeilleursVotes
                                   </div>
                               </div>
                            </div>
                       </div>
                   </div>
                </div>
END;


        echo $html;
    }


    public function testsDiagrammes($nbph) {

        if($nbph == 0) {
            return '<p><i>Aucune photo n\'a encore été postée.</i></p>';
        } else {

            if(Photographie::nombreDeVotesAuTotal() == 0) {
                return '<p><i>Aucun vote n\'a encore été posté.</i></p>';
            } else {
                return '';
            }
        }
    }


    public function partieCommuneDiagrammes() {

        $nbTotalVotes = Photographie::nombreDeVotesAuTotal();

        $html = '<p><i>Nombre total de votes depuis le début de l\'événement : ' . $nbTotalVotes . '</i></p><br><br>
                  <ul class="diagramme">';

        return $html;

    }


    public function obtenirDiagrammePlusDeVotes($nbph) {

        $html = $this->testsDiagrammes($nbph);
        $nbVotesMax = 1;

        if($html == '') {

            $html .= $this->partieCommuneDiagrammes();
            $listePhotos = Photographie::photosPlusVotees();

            foreach ($listePhotos as $idph => $nbVotes) {

                $idph = intval(explode('_',$idph)[1]);
                $ph = Photographie::find($idph);
                $nbVotes = count($ph->votes);

                if ($nbVotes != 0) {

                    if($nbVotes > $nbVotesMax) {
                        $nbVotesMax = $nbVotes;
                    }

                    $idPh = $ph->id;
                    $nomUser = $ph->user->name;
                    $width = ($nbVotes / $nbVotesMax) * 100;

                    $html .= '<li>
                                  <div>
                                      <span class="label">Photo n°' . $idPh . ' de ' . $nomUser . ' - ' . $nbVotes . ' Vote(s)</span>
                                      <span class="count" style="width: ' . $width . '%;">(' . $nbVotes . ')</span>
                                  </div>
                              </li>';
                    }
                }

                $html .= '</ul>';
            }

        return $html;

    }


    public function obtenirDiagrammeMeilleursVotes($nbph) {

        $html = $this->testsDiagrammes($nbph);

        if($html == '') {

            $html .= $this->partieCommuneDiagrammes();
            $listePhotos = Photographie::photosMeilleursVotes();

            foreach($listePhotos as $idph => $sommeVotes) {

                $idph = intval(explode('_',$idph)[1]);
                $ph = Photographie::find($idph);
                $nbVotes = count($ph->votes);

                if ($sommeVotes != 0) {

                    $idPh = $ph->id;
                    $nomUser = $ph->user->name;
                    $width1 = ($ph->retournerNbVotesSelonType('n')/$nbVotes) * 100;
                    $width2 = ($ph->retournerNbVotesSelonType('s')/$nbVotes) * 100;
                    $width3 = ($ph->retournerNbVotesSelonType('c')/$nbVotes) * 100;
                    $width4 = ($ph->retournerNbVotesSelonType('g')/$nbVotes) * 100;

                    $html .= '    <li class="li2" style="width:'. $width1 .'%;">
                                      <div>
                                          <span class="label">Photo n°' . $idPh . ' de ' . $nomUser . ' - ' . $nbVotes . ' Vote(s)' . ' - ' . $sommeVotes . ' pts</span>
                                          <span class="count count1"></span>
                                      </div>
                                  </li>
                                  <li class="li2" style="width: ' . $width2 . '%;">
                                      <div>
                                          <span class="count count2"></span>
                                      </div>
                                  </li>
                                  <li class="li2" style="width: ' . $width3 . '%;">
                                      <div>
                                          <span class="count count3"></span>
                                      </div>
                                  </li>
                                  <li style="width: ' . $width4 . '%; word-wrap: break-word;">
                                      <div>
                                          <span class="count count4"></span>
                                      </div>
                                  </li><pre></pre>';
                }
            }

            $html .='
                    </ul>

                    <ul class ="leg no-padding">
                        <li class="btn coulLegendeN leg">Bof (1pt)</li>
                        <li class="btn coulLegendeS leg">Sympa (2pts)</li>
                        <li class="btn coulLegendeC leg">Cool (3pts)</li>
                        <li class="btn coulLegendeG leg">Génial (4pts)</li>
                    </ul>
            ';


        }

        return $html;

    }

    protected function renderJSON($options = null)
    {
        // TODO: Implement renderJSON() method.
    }
}