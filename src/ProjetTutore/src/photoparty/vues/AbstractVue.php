<?php

namespace photoparty\vues;

abstract class AbstractVue
{

    protected $jsonMode;
    protected abstract function render($options = null);
    protected abstract function renderJSON($option = null);

    public function __construct($jsonMode = false){
        $this->jsonMode = $jsonMode;
    }

    public function globalRender($option = null) {
        if(!$this->jsonMode) {
            $this->headerRender();
            $this->render($option);
            $this->footerHeader();
        }else{
            $this->renderJSON($option);
        }
    }


    public function headerRender() {

        include('web/tpl/header.html');

    }


    public function footerHeader() {

        include('web/tpl/footer.html');

    }

}