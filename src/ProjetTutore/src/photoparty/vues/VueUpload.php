<?php

namespace photoparty\vues;

class VueUpload extends AbstractVue
{

    public function render($option = null) {
        switch($option) {
            case 1:
                echo '
                    <div id="message" class="container message">
                        <div class="alert alert-' . $_SESSION['flash']['alert'] . '">
                            ' . $_SESSION['flash']['message'] . '
                           <comment>Cliquez n\'importe où pour fermer la fenêtre.</comment>
                        </div>
                    </div>
                ';
                break;
        }

        include('web/tpl/index.html');

    }

    protected function renderJSON($options = null)
    {
        switch($options){
            case 1:
                echo json_encode($_SESSION['flash']);
                break;
        }
    }
}