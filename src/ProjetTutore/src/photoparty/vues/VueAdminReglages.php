<?php

namespace photoparty\vues;

use photoparty\modeles\Configuration;
use photoparty\modeles\Disque;
use photoparty\modeles\ListeStockageExterne;
use photoparty\modeles\Photographie;

class VueAdminReglages extends AbstractVue
{

    public function render($options = null) {

        $codeArrRepDiapo = $this->obtenirContenuArretRepriseDiapo();
        $codeTransitionDiapo = $this->obtenirContenuTransitionDiapo();
        $codeStockageDiapo = $this->obtenirContenuModeStockageDiapo();
        $codeEthernetDiapo = $this->obtenirContenuEthernetDiapo();
        $codeTransitionMask = $this->obtenirContenuTransition();
        $codeVignettesDiapo = $this->obtenirCodeVignettesPhotos();
        $codePassword = $this->obtenirChampMotDePasse();

        $html = <<<END
            <a role="button" data-role="button" id="sidelinkReglages" class="sidelink col-xs-1 ui-link ui-btn ui-shadow ui-corner-all" href="adminstat">
                <span>Statistiques</span>
            </a>

            <br><br><br>

                <div class="col-sm-6 col-xs-10 col-center-block blocAdmin">
                    <div class="panel panel-default">
                        <div id="pb" class="panel-body">
                            <span class="glyphicon glyphicon-cog iconAdmin"></span>
                            <h1 class="titreAdmin">Panneau d'Administration</h1>
                            <h3 class="sousTitreAdmin">Réglages du Diaporama</h3>
                            <br><br><br><br><br>

                            <form name="admin" method="post" action="adminreg">
                                <div class="panel panel-default panel-bordure divPanneauAdmin">
                                    <div class="panel-body">
                                        <span class="glyphicon glyphicon-off"></span>
                                        $codeArrRepDiapo
                                    </div>
                                </div>


                                <br><br>

                                <div class="panel panel-default panel-bordure divPanneauAdmin">
                                    <div class="panel-body">
                                        <span class="glyphicon glyphicon-hourglass"></span>
                                        $codeTransitionDiapo
                                    </div>
                                </div>

                                <br><br>

                                <div class="panel panel-default panel-bordure divPanneauAdmin">
                                    <div class="panel-body">
                                        <span class="glyphicon glyphicon-film"></span>
                                        $codeTransitionMask
                                    </div>
                                </div>

                                <br><br>

                                <div class="panel panel-default panel-bordure divPanneauAdmin">
                                    <div class="panel-body">
                                        <span class="glyphicon glyphicon-save"></span>
                                        $codeStockageDiapo
                                    </div>
                                </div>

                                <br><br>

                                <div class="panel panel-default panel-bordure divPanneauAdmin">
                                    <div class="panel-body">
                                        <span class="glyphicon glyphicon-globe"></span>
                                        $codeEthernetDiapo
                                    </div>
                                </div>

                                <br><br>

                                <div class="panel panel-default panel-bordure divPanneauAdmin">
                                    <div class="panel-body">
                                        <span class="glyphicon glyphicon-picture"></span>
                                        $codeVignettesDiapo
                                    </div>
                                </div>

                                <br><br>

                                <div class="panel panel-default panel-bordure divPanneauAdmin">
                                    <div class="panel-body">
                                        <span class="glyphicon glyphicon-lock"></span>
                                        $codePassword
                                    </div>
                                </div>

                                <br><br><br><br>

                                <div class="btnfix">
                                    <button id="boutonReglages" data-ajax="false" class="button btn btn-success btn-lg btn-form" type="submit" name="param" value="valid">Valider</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <script src="web/js/barre.js"></script>
                <script src="web/js/ethernet.js"></script>
                <script src="web/js/cloud.js"></script>
                <script src="web/js/cloud2.js"></script>
                <script src="web/js/vignettes.js"></script>
END;

        echo $html;
    }


    public function obtenirContenuArretRepriseDiapo() {

        $html = <<<END
           <h3>Arrêt / Reprise du diaporama</h3>
           <br><br>
           <div class="row">
            <div class="divCentr">
END;

        $diapoState = Configuration::getConfigValue(5);

        if($diapoState == 1) {
            $play = '<input id="bradio4" type="radio" name="etatdiapo" value="1" checked/>';
            $pause = '<input id="bradio5" type="radio" name="etatdiapo" value="0"/>';
        } else {
            $play = '<input id="bradio4" type="radio" name="etatdiapo" value="1"/>';
            $pause = '<input id="bradio5" type="radio" name="etatdiapo" value="0" checked/>';
        }

        $html .= <<<END
            $play
            <label class="col-xs-3 labelIn labelEtat" for="bradio4">
                <img class="cercle etatCercle" src="web/images/play.png"/>
            </label>

            $pause
            <label class="col-xs-3 labelIn labelEtat" for="bradio5">
                <img class="cercle etatCercle" src="web/images/pause.png"/>
            </label>
          </div>
        </div>
END;

        return $html;
    }


    public function obtenirContenuTransitionDiapo() {


        $html = '<h3>Temps de transition entre les photos</h3>
                <div class="row">
                    <label id="labelBarre" for="barre">
                        <p id="barreAff"></p>
                    </label>';

        $transitionTime = Configuration::getConfigValue(4)/1000;

        $html .= <<<END
             <input id="barre" type="range" value="$transitionTime" min="5" max="30" name="tempsTrans"/>
          </div>
END;

        return $html;

    }


    public function obtenirContenuModeStockageDiapo() {

        $html = '<h3>Mode de stockage</h3>
                <br><br>
                <div id="divStockage" class="row">';

        $stockageSelect = Configuration::getConfigValue(3);
        $sd = '';
        $dd = '';
        $cl = '';

        switch($stockageSelect) {
            case 1:
                $sd = '<input id="bradio1" type="radio" name="stockage" value="1" checked/>';
                $dd = '<input id="bradio2" type="radio" name="stockage" value="2"/>';
                $cl = '<input id="bradio3" type="radio" name="stockage" value="3"/>';
                break;

            case 2:
                $sd = '<input id="bradio1" type="radio" name="stockage" value="1" />';
                $dd = '<input id="bradio2" type="radio" name="stockage" value="2" checked/>';
                $cl = '<input id="bradio3" type="radio" name="stockage" value="3"/>';

                break;

            case 3:
                $sd = '<input id="bradio1" type="radio" name="stockage" value="1"/>';
                $dd = '<input id="bradio2" type="radio" name="stockage" value="2"/>';
                $cl = '<input id="bradio3" type="radio" name="stockage" value="3" checked/>';
                break;
        }

        $codeTabDisques = $this->obtenirContenuDisquesDispos();
        $codeCloud = $this->obtenirCodeCloud();

        $html .= <<<END
            $sd
            <label class="col-xs-3 labelIn" for="bradio1">
                <img class="cercle" src="web/images/cerclesd.png"/>
            </label>
            $dd
            <label class="col-xs-3 labelIn" for="bradio2">
                <img class="cercle" src="web/images/cercledd.png"/>
            </label>
            $cl
            <label class="col-xs-3 labelIn" for="bradio3">
                <img class="cercle" src="web/images/cerclecl.png"/>
            </label>

            <div id="info">
                <hr>
                $codeTabDisques
                $codeCloud

                <div id="champCloud">
                        <div id="divCloud1">
                            <input id="serCloud1" type="radio" name="serviceCloud" value="dropbox"/>
                                <label class="labelCloud" for="serCloud1">
                                    <img src="web/images/dropbox.png"/><br>
                                    <span class="label label-pill label-default btn">Dropbox</span>
                                </label>
                            </div>

                            <div id="divCloud2">
                                <input id="serCloud2" type="radio" name="serviceCloud" value="googledrive"/>
                                <label class="labelCloud" id="labelCloud2" for="serCloud2">
                                    <img src="web/images/drive.png"/><br>
                                    <span class="label label-pill label-default btn">Google Drive</span>
                                </label>
                            </div>

                            <div id="divCloud3">
                                <input id="serCloud3" type="radio" name="serviceCloud" value="onedrive"/>
                                <label class="labelCloud" id="labelCloud3 lastChampCloud" for="serCloud3">
                                    <img src="web/images/onedrive.png"/><br>
                                    <span class="label label-pill label-default btn">One Drive</span>
                                </label>
                            </div>
                        </div>

                        <div id="messageErreurStock" class="alert alert-danger" role="alert">
                            <span><img src="web/images/exclamation.png"/></span>
                            <span class="sr-only">Attention :</span>
                            Pour sélectionner le mode de stockage \'Cloud\',<br>l\'activation de la connexion ETHERNET est nécessaire.
                        </div>
           </div>
      </div>
END;

        return $html;

    }


    public function obtenirCodeCloud() {

        $html = '';

        if(file_exists("/tmp/cloudInstance.serial")) {

            $ci = unserialize(file_get_contents("/tmp/cloudInstance.serial"));
            $nomService = '';
            $nomImage = '';

            switch($ci->getClassName()) {

                case "DropBoxInstance":

                    $nomService = "DropBox";
                    $nomImage = "dropbox.png";

                    if(isset($_GET['oauth_token'])) {

                        $ci->createClient($_GET['oauth_token']);
                        file_put_contents("/tmp/cloudInstance.serial", serialize($ci));

                        echo "<script>document.location.href='adminreg'</script>";

                    }

                    break;

                case "OneDriveInstance":

                    $nomService = "OneDrive";
                    $nomImage = "onedrive.png";

                    if(isset($_GET['code'])) {

                        $ci->createClient($_GET['code']);
                        file_put_contents("/tmp/cloudInstance.serial", serialize($ci));

                        echo "<script>document.location.href='adminreg'</script>";

                    }

                    break;

                case "GoogleDriveInstance":

                    $nomService = "Google Drive";
                    $nomImage = "drive.png";

                    if(isset($_GET['code'])) {

                        $ci->createClientFromToken($_GET['code']);
                        file_put_contents("/tmp/cloudInstance.serial", serialize($ci));

                        echo "<script>document.location.href='adminreg'</script>";

                    }

                    break;

            }

            $nomUtilisateur = $ci->getUserName();
            $espaceUtilise = $ci->getUsedSpace("string");
            $espaceTotal = $ci->getTotalSpace("string");

            $html = '<div class="container" id="divCloudConnecte">
                            <span class="label label-pill label-default btn spanCloudConnecte" id="deconnexionCloud" title="Déconnexion" aria-hidden="true">Se Déconnecter</span>
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="imgCloudConnecte" src="web/images/' . $nomImage . '" />
                                    <h3>Connecté à ' . $nomService . ' !</h3>
                                    <p><i>' . $nomUtilisateur . '</i></p>
                                    <span class="label label-pill label-default btn spanCloudConnecte">' . $espaceUtilise . ' / ' . $espaceTotal . '</span>
                                </div>
                            </div>
                          </div>';

        }

        return $html;

    }


    public function obtenirContenuDisquesDispos() {

        $stockExt = new ListeStockageExterne();
        $stockExt->mettreAjourDisques();
        $tabDisques = $stockExt->getDisquesDisponibles();
        //$tabDisques = [Disque::creerDisqueVirtuel(50,30),Disque::creerDisqueVirtuel(12,9),Disque::creerDisqueVirtuel(33,0)];

        if(count($tabDisques) >= 1) {

            $codeTabDisques = '<div id="disquesDispos">';

            foreach ($tabDisques as $disque) {

                $codeTabDisques .= '     <ul class="list-group">
                                            <li class="list-group-item premiereInfoDisque">
                                                <input class="inputDisque" id="' . $disque->label . '" type="radio" name="disque" value="' . $disque->dossierRacine . '" checked/>
                                                <span class="label label-default label-pill pull-xs-right">'. $disque->label .'</span>
                                            </li>
                                            <li class="list-group-item infoDisque">
                                                Capacité Totale -
                                                <span class="label label-default label-pill pull-xs-right">' . $disque->capaciteTot .'o</span>
                                            </li>
                                            <li class="list-group-item infoDisque">
                                                Capacité Utilisée -
                                                <span class="label label-default label-pill pull-xs-right">' . $disque->capaciteUtil .'o</span>
                                            </li>
                                            <li class="list-group-item infoDisque">
                                                Capacité Disponible -
                                                <span class="label label-default label-pill pull-xs-right">' . $disque->capaciteDispo .'o</span>
                                            </li>
                                            <li class="list-group-item infoDisque derniereInfoDisque">
                                                Pourcentage Utilisé -
                                                <span class="label label-default label-pill pull-xs-right">' . $disque->pourcUtil .'</span>
                                            </li>
                                        </ul>';
            }

            $codeTabDisques .= '</div>';
        } else {
            $codeTabDisques = '<div id="messageErreurDisques" class="alert alert-danger" role="alert">
                                    <span><img src="web/images/exclamation.png"/></span>
                                    <span class="sr-only">Attention :</span>
                                    Pour sélectionner le mode de stockage \'Disque Dur\',<br> vous devez connecter un périphérique. <br>
                                    Veuillez recharger après la connexion d\'un périphérique
                               </div>';
        }

        return $codeTabDisques;

    }


    public function obtenirContenuEthernetDiapo() {

        $html = <<<END
            <h3>Activation d'Ethernet</h3>
            <br><br>
            <div class="row">
                <div class="divCentr">

END;

        $ethernetState = Configuration::getConfigValue(6);

        if($ethernetState == 1) {
            $eth = '<input id="ethernet" type="checkbox" name="ethernet" value="eth" checked/>
                    <label id="labelEth" class="col-xs-3 labelIn" for="ethernet">
                        <img id="imgEth" src="web/images/ethernet1.png"/>
                    </label>';
        } else {
            $eth = '<input id="ethernet" type="checkbox" name="ethernet" value="eth"/>
                    <label id="labelEth" class="col-xs-3 labelIn" for="ethernet">
                        <img id="imgEth" src="web/images/ethernet2.png"/>
                    </label>';
        }

        $html .= <<<END
            $eth
           </div>
        </div>
END;

        return $html;

    }


    public function obtenirContenuTransition(){
        $html = <<<END
        <h3>Style de transition</h3>
        <br><br>
        <div class="row">
            <div class="divCentr">
END;
        foreach(Configuration::getTransitionMask() as $key=>$value){

            $html.='<div class="checkbox">
                        <div class="[ form-group ]">
                            <input type="checkbox"  name="'.$key.'" value="on" id="in'. $key .'" autocomplete="off"';

            if($value)
                $html.=' checked/>';
            else
                $html.='/>';

            $html .= '<div class="[ btn-group ]">
                        <label for="in'.$key.'" class="[ btn btn-success btn-transition ]">
                            <span class="[ glyphicon glyphicon-ok ]"></span>
                            <span> </span>
                        </label>
                        <label for="in'.$key.'" class="[ btn btn-default active ]">
                            '. $key .'
                        </label>
                     </div>
                  </div>
               </div>';
        }

        $html.= <<<END
            </div>
        </div>
END;

        return $html;
    }


    public function obtenirCodeVignettesPhotos() {

        $html = '<div id="monancre"></div>
                <h3>Suppression de photos</h3>
                <br><br>
                <div class="row" id="divVignettes">';

        $tabVignettes = Photographie::obtenirPhotosVignettes();

        if(count($tabVignettes) == 0) {
            $html .= '<p><i>Aucune photo n\'a encore été postée !</i></p><br><br>';
        } else {
            $html .= '<p><i>Cliquez sur les photos que vous souhaitez supprimer : </i></p><br><br>';
        }

        if(empty($_GET['p'])) {
            $p = 1;
        } else {
            $p = (int)$_GET['p'];
        }


        $nbPhotosAffichees = 1;
        $nbRows = count($tabVignettes);
        $nbDisplay = 6;
        $start = $p*$nbDisplay - $nbDisplay;
        $pagination = (int)ceil($nbRows/$nbDisplay);
        $nbPhotosNonAffichees = 0;

        //Affichage des vignettes selon le nombre de vignettes par page et le nombre de vignettes
        foreach($tabVignettes as $vign) {
            if(($nbPhotosNonAffichees >= $start) && ($nbPhotosAffichees <= $nbDisplay)) {
                $tmp = explode("/",$vign);
                $nom = end($tmp);
                $html .= '<input id="vignette' . $nbPhotosAffichees . '" type="checkbox" name="vignette'. $nbPhotosAffichees .'" value="' . $nom . '"/>
                          <label id="labelVign' . $nbPhotosAffichees . '" class="col-xs-3 labelIn labelVign no-padding" for="vignette' . $nbPhotosAffichees . '">
                            <img class="imgVign" id="imgVign' . $nbPhotosAffichees . '" src="web/images/vignettes/' . $nom . '"/>
                            <span class="glyphicon glyphicon-remove-circle icon"></span>
                          </label>';
                $nbPhotosAffichees++;
            }

            $nbPhotosNonAffichees++;
        }

        $html .= '<br><br>';
        $customPagination = 0;
        while($customPagination < $pagination) {
            $html .= '<span class="pagination"><a class="lienPagi" href="?p='. ($customPagination+1) . '">' . ($customPagination+1) . '</a></span>';
            $customPagination++;
        }

        $html .= '</div>';

        return $html;

    }

    protected function renderJSON($options = null)
    {
        // TODO: Implement renderJSON() method.
    }

    public function obtenirChampMotDePasse() {

        $html = '<h3>Changement du mot de passe administrateur</h3>
                <br><br>
                <div class="row">
                    <div class="form-group">
                        <label class="sr-only" for="changMDP">Mot de passe actuel :</label>
                        <input name="champPassActu" type="password" class="form-control champPass" placeholder="Mot de passe actuel">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="changMDP">Nouveau mot de passe :</label>
                        <input name="champPassNouv" type="password" class="form-control champPass" placeholder="Entrez un nouveau mot de passe">
                    </div>
                </div>';

        if(isset($_SESSION['flash']) && !empty($_SESSION['flash']))
        {
            $html.='<div class="alert-'.$_SESSION['flash']['alert'].'">'.$_SESSION['flash']['message'].'</div>';
        }
        return $html;

    }
}