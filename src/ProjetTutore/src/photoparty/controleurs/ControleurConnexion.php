<?php

namespace photoparty\controleurs;

use Illuminate\Database\QueryException;
use photoparty\modeles\Configuration;
use photoparty\util\PhpCommunication;
use photoparty\vues\VueConnexion as VueConnexion;
use photoparty\modeles\Utilisateur as Utilisateur;
use photoparty\modeles\Stockage as Stockage;
use Slim\Http\Util;
use Slim\Slim;

class ControleurConnexion extends AbstractControler
{

    public function afficherFormulaireConnexion()
    {
        if(!$this->estConnecte()){
            $vue = new VueConnexion();
            $vue->globalRender();
        } else {
            if($_SESSION['utilisateur']->isAdmin())
                \Slim\Slim::getInstance()->redirectTo('adminreg');
            else
                \Slim\Slim::getInstance()->redirectTo('upload');
        }
    }

    public function seConnecter() {

        $data = $this->request->post();

        $json = (isset($data['mode']) && $data['mode'] == 'json');
        if(isset($data['bouton']) && $data['bouton'] == 'valid'){
            if (
                /*!isset($_SESSION['utilisateur'])*/
            !$this->estConnecte())
            {
                $ip = $this->request->getIp();
                $util = new Utilisateur();
                $util->ipAddress = filter_var($this->getMac($ip),FILTER_SANITIZE_STRING);
                $util->name = $data['pseudo'];
                if ($util->name == Configuration::getConfigValue(Configuration::ADMIN_PSEUDO)) {
                    if (isset($data['password']) && !empty($data['password'])) {
                        if ($util->elevatePrivilege($data['password'])){
                            if(is_null(Utilisateur::find($util->ipAddress)))
                                $util->save();
                            $_SESSION['utilisateur'] = $util;
                            \Slim\Slim::getInstance()->redirectTo('adminreg');
                        } else {
                            $_SESSION['flash'] = ['alert' => 'danger', 'message' => '<strong>Erreur !</strong> Mot de passe incorrect'];
                            $vue = new VueConnexion($json,true);
                            $vue->globalRender();
                        }
                    } else {
                        $vue = new VueConnexion($json,true);
                        $vue->globalRender();
                    }
                } else {
                    try {
                        $util->save();
                        $_SESSION['utilisateur'] = $util;
                        if(!$json) {
                            \Slim\Slim::getInstance()->redirectTo('upload');
                        }else{
                            $message = ["alert" => "success","message" => 'Connexion reussie'];
                            echo json_encode($message);
                        }
                    } catch (QueryException $ex) {
                        $_SESSION['flash'] = ['alert' => 'danger', 'message' => '<strong>Erreur !</strong> Utilisateur déjà présent'];
                        //\Slim\Slim::getInstance()->redirectTo('connexion');
                        $vue = new VueConnexion($json,false);
                        $vue->globalRender();
                    }
                }
            }else{
                $_SESSION['utilisateur'] = Utilisateur::find($this->getMac($this->request->getIp()));
                if(!$json) {
                    if ($_SESSION['utilisateur']->isAdmin())
                        \Slim\Slim::getInstance()->redirectTo('adminreg');
                    else
                        \Slim\Slim::getInstance()->redirectTo('upload');
                }else {
                    $message = ["alert"=> "success","message" => 'Vous êtes déjà connecté'];
                    echo json_encode($message);
                }
            }
        }
    }

}