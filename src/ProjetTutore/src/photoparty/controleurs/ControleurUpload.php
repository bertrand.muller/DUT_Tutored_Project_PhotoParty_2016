<?php

namespace photoparty\controleurs;

use photoparty\util\PhpCommunication;
use photoparty\vues\VueUpload as VueUpload;

class ControleurUpload extends AbstractControler
{

    public function afficherFormulaireUpload(){
       if($this->estConnecte()) {
            $vue = new VueUpload();
            $vue->globalRender();
        }else{
            \Slim\Slim::getInstance()->redirectTo('connexion');
        }

    }


    public function uploader()
    {
        if($this->estConnecte()) {
            $util = $_SESSION['utilisateur'];
            $util->recevoirPhoto();

            $data = $this->request->post();
            $jsonMode = isset($data['mode']) && $data['mode'] == 'json';
            $vue = new VueUpload($jsonMode);
            $vue->globalRender(1);

        }


    }


}