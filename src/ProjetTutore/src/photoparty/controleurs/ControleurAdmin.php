<?php

namespace photoparty\controleurs;


use Illuminate\Support\Facades\Config;
use photoparty\modeles\Configuration;
use photoparty\modeles\DropBoxInstance;
use photoparty\modeles\GoogleDriveInstance;
use photoparty\modeles\OneDriveInstance;
use photoparty\modeles\Photographie;
use photoparty\modeles\Stockage;
use photoparty\util\PhpCommunication;
use photoparty\vues\VueAdminReglages;
use photoparty\vues\VueAdminStatistiques;
use photoparty\vues\VueValidationReglages;

if(isset($_GET['eth'])) {

    $ind = filter_var($_GET['eth'], FILTER_SANITIZE_NUMBER_INT);

    if($ind == 1){

        Configuration::setConfigValue(Configuration::ETHERNET_STATE,1);
        exec("sudo /root/ethernet_powerUp.sh");

    } else {

        Configuration::setConfigValue(Configuration::ETHERNET_STATE, 0);
        exec("sudo /root/ethernet_powerDown.sh");

    }
}

if(isset($_GET['typeCloud'])) {

    $type = $_GET['typeCloud'];

    Configuration::setConfigValue(Configuration::STOCKAGE_SELECT,1);

    $newStockage = new Stockage();
    $newStockage->prefix = "/var/www/uploads";
    $newStockage->type = "DisqueDur";
    $newStockage->save();

    Configuration::setConfigValue(Configuration::STOCKAGE_SELECT,$newStockage->id);

    if($type == "dropbox") {

        file_put_contents("/tmp/cloudInstance.serial",serialize(new DropBoxInstance()));

    } else if($type == "google") {

        file_put_contents("/tmp/cloudInstance.serial",serialize(new GoogleDriveInstance()));

    } else {
        file_put_contents("/tmp/cloudInstance.serial",serialize(new OneDriveInstance()));
    }

}

if(isset($_GET['cloudDeconnexion'])) {
    if($_GET['cloudDeconnexion'] == 1) {
        exec("rm /tmp/cloudInstance.serial");
    }
}

class ControleurAdmin extends AbstractControler
{
    public function formulaireAdminReglages(){
        if($this->estConnecte()){
            if($_SESSION['utilisateur']->isAdmin()) {
                $v = new VueAdminReglages();
                $v->globalRender();
            }
        }
    }

    public function formulaireAdminStatistiques() {
        if($this->estConnecte()) {
            if($_SESSION['utilisateur']->isAdmin()) {
                $photos = Photographie::all();
                $v = new VueAdminStatistiques($photos);
                $v->globalRender();
            }
        }
    }


    public function validerFormulaireAdmin() {
        if($this->estConnecte()) {
            if($_SESSION['utilisateur']->isAdmin()) {

                $data = $this->request->post();

                if (isset($data['param']) && $data['param'] == 'valid') {

                    $erreur = false;
                    Configuration::setConfigValue(Configuration::STOCKAGE_SELECT,$data['stockage']);
                    Configuration::setConfigValue(Configuration::TRANSITION_TIME,$data['tempsTrans']*1000);
                    Configuration::setConfigValue(Configuration::DIAPORAMA_STATE,$data['etatdiapo']);

                    if(isset($data['disque']) && $data['stockage']==2) {
                        $dossierRacine = $data['disque'];
                        $newStockage = new Stockage();
                        $newStockage->prefix = $dossierRacine;
                        $newStockage->type = "DisqueDur";
                        $newStockage->save();
                        Configuration::setConfigValue(Configuration::STOCKAGE_SELECT,$newStockage->id);
                    }

                    foreach(Configuration::$transitions as $key=>$value){
                        if(isset($data[str_replace(" ","_",$key)]))
                            Configuration::setTransition($key);
                        else
                            Configuration::clearTransition($key);
                    }

                    foreach($data as $key => $input) {
                        if(strstr($key,"vignette")){
                            $input = explode("_",$input)[1];
                            $input = explode(".",$input)[0];
                            $ext = explode(".",Photographie::find($input)->path)[1];
                            $basepath = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);
                            unlink($basepath.'/web/images/vignettes/thumb_'.$input.'.'.$ext);
                            PhpCommunication::getInstance()->supprimerPhoto($input);
                            Photographie::find($input)->delete();
                        }
                    }

                    if(isset($data['champPassActu']) && isset($data['champPassNouv'])) {
                        if(preg_match("/[a-zA-Z0-9:punct:]+/",$data['champPassActu']) && preg_match("/[a-zA-Z0-9:punct:]+/",$data['champPassActu'])) {
                            $erreur = !Configuration::changePassword($data['champPassActu'], $data['champPassNouv']);
                        }
                    }

                    if(!$erreur) {
                        PhpCommunication::getInstance()->configHasChanged();

                        $v = new VueValidationReglages();
                        $v->globalRender();
                    }else{
                        $_SESSION['flash'] = array(
                            "alert"=>"danger",
                            "message"=>"Mauvais mot de passe admin"
                        );
                        $v = new VueAdminReglages();
                        $v->globalRender();
                    }
                }
            }
        }
    }
}