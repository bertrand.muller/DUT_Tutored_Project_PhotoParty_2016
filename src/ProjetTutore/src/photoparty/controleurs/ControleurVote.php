<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/01/16
 * Time: 22:45
 */

namespace photoparty\controleurs;


use photoparty\modeles\Photographie;
use photoparty\vues\VueVote;

class ControleurVote extends AbstractControler
{
    public function formulaireVote(){
        if($this->estConnecte()){
            $v = new VueVote();
            $v->globalRender();
        }else{
            \Slim\Slim::getInstance()->redirectTo('connexion');
        }
    }

    public function voter(){
        if($this->estConnecte()){
            $data = $this->request->post();
            if(isset($data['vote']) && $data['vote']=='valid'){
                $avis = $data['avis'];
                $votes = $_SESSION['utilisateur']->votes();

                $_SESSION['utilisateur']->voter($avis);
            }

            $jsonMode = isset($data['mode']) && $data['mode'] == 'json';
            $v = new VueVote($jsonMode);
            $v->globalRender(1);
        }
    }
}