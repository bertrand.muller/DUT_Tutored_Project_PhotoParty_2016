<?php

namespace photoparty\controleurs;

use photoparty\modeles\Configuration;
use photoparty\modeles\Utilisateur;

class AbstractControler
{

    protected $request;

    public function __construct($request){
        $this->request = $request;
    }

    public function estConnecte() {
        if(isset($_SESSION) && !empty($_SESSION) && isset($_SESSION['utilisateur'])){
            return true;
        }else{
            $user = Utilisateur::find($this->getMac($this->request->getIp()));
            if($user != null) {
                if ($user->name == Configuration::getConfigValue(Configuration::ADMIN_PSEUDO))
                    return false;
                else {
                    $_SESSION['utilisateur'] = $user;
                    return true;
                }
            }else{
                return false;
            }
        }
    }

    public function getMac($ip){
        $ret = shell_exec("arp -an ".escapeshellarg($ip)." | cut -d ' ' -f 4");
        if($ret != null && strlen($ret)==18)
            return $this->hashMac($ret);
        else
            return $this->hashMac("FF:FF:FF:FF:FF:FF");
    }

    private function hashMac($mac){
        $hex = explode(":",$mac);
        $id = 0;
        foreach($hex as $s){
            $id  = ($id << 2) & 0xFFFFFFFF;
            $id += hexdec($s) & 0xFFFFFFFF;
        }
        return $id;
    }
}