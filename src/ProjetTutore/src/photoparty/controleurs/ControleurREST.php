<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 27/03/16
 * Time: 16:36
 */

namespace photoparty\controleurs;


use photoparty\util\PhpCommunication;

class ControleurREST extends AbstractControler
{

    public function afficherConfig(){
        $json = array(
            "width" => PhpCommunication::getInstance()->getScreenWidth(),
            "height" => PhpCommunication::getInstance()->getScreenHeight(),
            "current" => PhpCommunication::getInstance()->getCurrentPhoto()
            );
        if($this->estConnecte()){
            $json['nom'] = $_SESSION['utilisateur']->name;
        }
        echo json_encode($json);
    }
}