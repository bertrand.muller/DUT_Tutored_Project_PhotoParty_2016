<?php 
echo "*** Test NamedPipeModule ***\n";
$handle = create_named_pipe_client("\\\\.\\pipe\\mynamedpipe");
echo "Handle value is : $handle\n";
$ret = write_file($handle,"Client");
echo "WriteFile return $ret";
$ret = read_file($handle);
echo "ReadFile return $ret\n";
$ret = close_handle($handle);
echo "close_handle return $ret\n";
?>