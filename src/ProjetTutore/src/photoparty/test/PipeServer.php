<?php 
echo "*** Test NamedPipeModule ***\n";
$handle = create_named_pipe_server("\\\\.\\pipe\\mynamedpipe");
echo "Handle value is : $handle\n";
$ret = connect_named_pipe($handle);
echo "ConnectNamedPipe return $ret\n";
if($ret){
	$ret = read_file($handle);
	echo "ReadFile return $ret\n";
	$ret = write_file($handle,"Server");
	echo "WriteFile return $ret";
	flush_file_buffers($handle);
	disconnect_named_pipe($handle);
}
$ret = close_handle($handle);
echo "close_handle return $ret\n";
?>