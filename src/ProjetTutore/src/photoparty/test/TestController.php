<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/01/16
 * Time: 08:23
 */

namespace photoparty\test;

use photoparty\modeles\Photographie;

use Illuminate\Database\Capsule\Manager as DB;
use photoparty\modeles\Stockage;
use photoparty\modeles\Utilisateur;
use photoparty\util\PhpCommunication;

class TestController extends \photoparty\controleurs\AbstractControler
{
    private function reset()
    {
        DB::table('Vote')->delete();
        DB::table('Photographie')->delete();
        DB::table('Stockage')->delete();
        DB::table('User')->delete();
        DB::connection()->unprepared('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'Photographie\';');
        DB::connection()->unprepared('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'Stockage\';');

        $stockage = new Stockage();
        $stockage->prefix = '/var/www/uploads';
        $stockage->type = 'CarteSD';
        $stockage->save();
    }

    public function testBDD(){
        $this->reset();

        $user = new Utilisateur();
        $user->name = 'Tomtombinary';
        $user->ipAddress = $this->parseIpToBlob($this->request->getIp());
        $user->save();

        $u2 = new Utilisateur();
        $u2->name = 'Mathieu';
        $u2->ipAddress = $this->parseIpToBlob('1.1.1.1');
        $u2->save();

        $photo = new Photographie();
        $photo->path = 'MaPhoto.jpg';
        $photo->hash = MD5('HASH');
        $photo->id_stockage = 1;
        $photo->ip_user = $user->ipAddress;
        $photo->save();

        $p2 = new Photographie();
        $p2->path = 'Xorg.png';
        $p2->hash = MD5('HASH');
        $p2->id_stockage = 1;
        $p2->ip_user = $u2->ipAddress;
        $p2->save();

        $photo = $photo->find(1);
        echo 'id photographie : '.$photo->id.'<br>';
        echo 'path : '.$photo->path.'<br>';
        echo 'hash : '.$photo->hash.'<br>';
        echo 'prefix du stockage : '.$photo->stockage->prefix.'<br>';
        echo 'nom de l\'utilisateur : '.$photo->user->name.'<br>';

        $user->votes()->attach($photo->id,array("avis" => '+'));
        $user->votes()->attach($p2->id,array("avis" => '-'));
        $u2->votes()->attach($photo->id,array("avis" => '-'));

        echo $user->name.':<br>';
        foreach($user->votes as $p){
            echo ' a voté '.$p->pivot->avis.' pour '.$p->path.'<br>';
        }

        echo $photo->path.':<br>';
        foreach($photo->votes as $u){
            echo $u->name.' a voté '.$u->pivot->avis.' pour cette photo <br>';
        }

        $this->reset();
    }

    public function testPhotoCourrante(){
        echo 'Id de la photocourrante : '.PhpCommunication::getInstance()->getCurrentPhoto();
    }

    public function testWindowsPipe(){
        echo "*** Test NamedPipeModule ***<br>";
        $handle = create_named_pipe_client("\\\\.\\pipe\\mynamedpipe");
        echo "Handle value is : $handle<br>";
        $ret = write_file($handle,"Client");
        echo "WriteFile return $ret<br>";
        $ret = read_file($handle);
        echo "ReadFile return $ret<br>";
        $ret = close_handle($handle);
        echo "close_handle return $ret<br>";
    }


    public function test($id){
        switch($id){
            case 1:
                $this->testBDD();
                break;
            case 2:
                $this->testPhotoCourrante();
                break;
            case 3:
                $this->testWindowsPipe();
                break;
            case 4:
                $this->testWindowsServerPipe();
                break;
        }
    }
}