<?php

namespace photoparty\modeles;


use Illuminate\Database\Eloquent\Model;

class Stockage extends Model
{
    protected $table='Stockage';
    protected $primaryKey='id';
    public $timestamps=false;

    public function photographies(){
        return $this->hasMany('\photoparty\modeles\Photographie','id_stockage','id');
    }


}