<?php
namespace photoparty\modeles;

/**
 * Déclaration des fichiers nécéssaires au fonctionnement
 *
 * APPLICATION_NAME : nom de l'application
 *
 * CREDENTIALS_PATH : répertoire de stockage des clés utilisateurs
 *
 * CLIENT_SECRET_PATH : clés d'authentification de l'API
 */

use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;

define('APPLICATION_NAME', 'PhotoParty');
define('CLIENT_SECRET_PATH', 'conf/googleDrive.json');


/**
 * Définition des droits demandés
 *
 *  DRIVE = All permissions
 */

define('SCOPES', implode(' ', array(
        Google_Service_Drive::DRIVE)
));


class GoogleDriveInstance extends AbstractCloudInstance {

    private $client;
    private $service;

    public function __construct()
    {

        $this->client = new Google_Client();
        $this->client->setApplicationName(APPLICATION_NAME);
        $this->client->setScopes(SCOPES);
        $this->client->setAuthConfigFile(CLIENT_SECRET_PATH);
        $this->client->setAccessType('offline');

        $authUrl = $this->client->createAuthUrl();
        echo json_encode(array('url' => $authUrl));

    }


    public function createClientFromToken($code){

        $this->client->authenticate($code);
        $this->service = new Google_Service_Drive($this->client);

        return $this->client;


    }

    public function getClient(){

        return $this->client;

    }


    public function getClientService(){

       return $this->service;

    }

    public function getFichier($identificateur){

        return $this->service->files->get($identificateur);

    }


    public function getListeFichiers(){


        $res = array();

        $optParams = array(
            'pageSize' => 10,
            'fields' => "nextPageToken, files(id, name)"
        );


        $allDataTmp =  $this->service->files->listFiles($optParams);

        if(count($allDataTmp->getFiles()) != 0){

            foreach($allDataTmp->getFiles() as $file){

                $res[$file->getName()] = $file->getId();

            }


        }

        return $res;

    }



    public function envoyerPhoto($pathTofile){


        $tmpArray = explode(".", $pathTofile);
        $type = end($tmpArray);

        $tmpArray = explode('/', explode('.', $pathTofile)[0]);
        $nom = end($tmpArray);

        $file = new Google_Service_Drive_DriveFile(array(

            'name' => $nom,
            'mimeType' => 'image/' . $type

        ));


        $data = file_get_contents($pathTofile);

        $this->service->files->create($file, array(

            'data' => $data,
            'mimeType' => 'image/' . $type,
            'uploadType' => 'multipart',
        ));

    }


    public function getUserName(){

        $optParams = array(
            'fields' => "user"
        );


        return $this->service->about->get($optParams)['modelData']['user']['displayName'];

    }


    public function getTotalSpace($format = 'number'){

        $optParams = array(
            'fields' => "storageQuota"
        );


        $espaceLibre = $this->service->about->get($optParams)['modelData']['storageQuota']['limit'];


        if($format  != 'number'){


            $espaceLibre = $this->byteConverter($espaceLibre);

        }


        return $espaceLibre;

    }


    public function getUsedSpace($format = 'number'){

        $optParams = array(
            'fields' => "storageQuota"
        );


      $espaceUtilise = $this->service->about->get($optParams)['modelData']['storageQuota']['usage'];


      if($format != 'number'){


          $espaceUtilise = $this->byteConverter($espaceUtilise);

      }


       return $espaceUtilise;

    }



    public function getFreeSpace($format = 'number')
    {

        $espaceLibre = $this->getTotalSpace() - $this->getUsedSpace();


        if ($format != 'number'){


            $espaceLibre = $this->byteConverter($espaceLibre);

        }

        return $espaceLibre;


    }


    public function getClassName()
    {

        return "GoogleDriveInstance";

    }
}
