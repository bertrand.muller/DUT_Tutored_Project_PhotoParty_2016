<?php

namespace photoparty\modeles;


class Disque {

	private $nomPhy,$label,$dossierRacine, $capaciteTot, $capaciteUtil, $capaciteDispo, $pourcUtil;

	static function creerDisquePhysique($n){

		$disque = new Disque();

		$infos = $disque->obtenirInformations($n);
        if(count($infos) == 6) {
            $disque->nomPhy = $infos[0];
            $disque->label = $disque->obtenirNomPartition($n);
            $disque->capaciteTot = $infos[1];
            $disque->capaciteUtil = $infos[2];
            $disque->capaciteDispo = $infos[3];
            $disque->pourcUtil = $infos[4];
            $disque->dossierRacine = $infos[5];
        }

        return $disque;
	}

	static function creerDisqueVirtuel($capacTot, $capacUtil){


		$disque = new Disque();

		$disque->nomPhy = "VirtualDisk";
		$disque->label = "Virtual_Disk" . rand(1,10);
		$disque->capaciteTot = $capacTot;
		$disque->capaciteUtil = $capacUtil;
		$disque->capaciteDispo= $capacTot - $capacUtil;
		$disque->pourcUtil= (($disque->capaciteDispo / $capacTot) * 100) . "%";
		$disque->dossierRacine = "/media/usb" . rand (5,10);
			
		return $disque;


	}


	private function obtenirInformations($n){
		$res = exec("df -h | grep '$n'");
		return preg_split('/\s+/', $res);
	
	}


	private function obtenirNomPartition($n){

		$infoPart = preg_split('/\s+/', exec("blkid | grep '$n'"));
		
		if(isset($infoPart[1])){
		
			return str_replace('"', '', str_replace('LABEL=', '', $infoPart[1]));
			
		} else {
		
			return "USB_DISK";
		}
	}
	
	public function __get($attname) {
	
		if (property_exists ($this, $attname) ) {
		
			return $this->$attname ; 
			
		} else {
		
			throw new Exception ("$attname : invalid property") ;
		
		}
	}


}
