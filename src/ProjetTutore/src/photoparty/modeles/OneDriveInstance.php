<?php

namespace photoparty\modeles;

class OneDriveInstance extends AbstractCloudInstance
{

    private $client;
    private $authInfos;
    private $state;


    public function __construct(){


        $this->authInfos = json_decode(file_get_contents("conf/oneDrive.json"), true);

        $onedrive = new \Krizalys\Onedrive\Client(array(

           'client_id' => $this->authInfos['id']

        ));


        $this->state = $onedrive->getState();


        $authUrl = $onedrive->getLogInUrl(array(

            'wl.signin',
            'wl.basic',
            'wl.skydrive_update'

        ), $this->authInfos['uri']);

        echo json_encode(array('url' => $authUrl));

    }

    public function createClient($code){


        $this->client = new \Krizalys\Onedrive\Client(array(

            'client_id' => $this->authInfos['id'],
            'state' => $this->state

            ));

        $this->client->obtainAccessToken($this->authInfos['secret'], $code);

        $this->state = $this->client->getState();

    }


    private function refreshClient(){


        $this->client = new \Krizalys\Onedrive\Client(array(

            'state' => $this->state

        ));

    }


    public function getListeFichiers()
    {

        $res = array();

        $this->refreshClient();

        $elems = $this->client->fetchObjects(null);

        foreach($elems as $file){

            if(!($file->isFolder())){

                $res[$file->getId()] = $file->getName();



            }

        }

        return $res;
    }

    public function getFichier($id)
    {

        $this->refreshClient();

        return $this->client->fetchObject($id);

    }

    public function envoyerPhoto($pathTofile)
    {

        $this->refreshClient();

        $tmpArray = explode("/", $pathTofile);
        $nom = end($tmpArray);

        $parent = $this->client->fetchObject(null);
        $parent->createFile($nom, file_get_contents($pathTofile));


    }

    public function getUserName()

    {

        $this->refreshClient();
        return str_replace('.','',$this->client->fetchAccountInfo()->name);
    }



    public function getTotalSpace($format = 'number'){

        $this->refreshClient();

        $espaceLibre = $this->client->fetchQuota()->quota;


        if($format  != 'number'){


            $espaceLibre = $this->byteConverter($espaceLibre);

        }


        return $espaceLibre;


    }

    public function getUsedSpace($format = 'number'){

        $this->refreshClient();

        $espaceUtilise =  - ($this->client->fetchQuota()->available - $this->client->fetchQuota()->quota);

        if($format != 'number'){


            $espaceUtilise = $this->byteConverter($espaceUtilise);

        }


        return $espaceUtilise;



    }


    public function getFreeSpace($format = 'number')
    {

        $this->refreshClient();

        $espaceLibre = $this->client->fetchQuota()->available;


        if ($format != 'number'){


            $espaceLibre = $this->byteConverter($espaceLibre);

        }

        return $espaceLibre;


    }

    public function getClassName()
    {
        return "OneDriveInstance";
    }
}