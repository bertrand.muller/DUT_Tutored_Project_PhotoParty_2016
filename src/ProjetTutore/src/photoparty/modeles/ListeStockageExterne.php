<?php
namespace photoparty\modeles;


use photoparty\modeles\Disque;

class ListeStockageExterne {

	private $tabDisque;
	
	
	function __construct(){
	
		$this->mettreAjourDisques();
	
	}
	
	
	public function mettreAjourDisques(){
	
		$this->tabDisque = array();
		exec("ls /dev/sd* | grep '[1-9]$'", $ldisques, $status);
		
		if($status == 0){
			
			for ($i = 0 ; $i < count($ldisques) ; $i++){
			
				$this->tabDisque[] = Disque::creerDisquePhysique($ldisques[$i]);
				
			}
		}
	}
	
	
	public function getDisquesDisponibles(){
	
		return $this->tabDisque;
	
	}


}	
