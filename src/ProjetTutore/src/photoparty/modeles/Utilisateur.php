<?php

namespace photoparty\modeles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use photoparty\util\PhpCommunication;
use photoparty\vues\VueUpload;
use Slim\Slim;

class Utilisateur extends Model
{
    protected $table='User';
    protected $primaryKey='ipAddress';
    public $timestamps=false;

    private $privilege = 0;
    
    public function recevoirPhoto()
    {
        $stockage = Configuration::stockage();
        $photographie = new Photographie();
        $photographie->path = pathinfo($_FILES['inputfile']['name'])['filename'].'.png';
        $photographie->ip_user = $_SESSION['utilisateur']->ipAddress;
        $photographie->id_stockage = $stockage->id;

        $uploaddir = $stockage->prefix;

        $uploadfile = $uploaddir.DIRECTORY_SEPARATOR.basename($photographie->path);

        $error = false;
        $photographie->hash = hash_file('md5',$_FILES['inputfile']['tmp_name']);
        try {

            $this->redimensionerPhotographie($_FILES['inputfile']['tmp_name'], $uploadfile);
            $size = getimagesize($uploadfile);
            $photographie->width = $size[0];
            $photographie->height = $size[1];
            $photographie->save();
            PhpCommunication::getInstance()->informerProjecteur($photographie->id);


        }catch(QueryException $ex){
            $error = true;
            $_SESSION['flash'] = array('alert'=> 'danger','message' => '<h2>Erreur</h2><p>Votre photo est déjà présente !</p>');
        }

        /*
        if(move_uploaded_file($_FILES['inputfile']['tmp_name'],$uploadfile)){
            $photographie->hash = hash_file('md5',$uploadfile);
            try {
                $photographie->save();
                PhpCommunication::getInstance()->informerProjecteur($photographie->id);
            }
            catch(QueryException $ex){
                $error = true;
                $_SESSION['flash'] = array('alert'=> 'danger','message' => '<h2>Erreur</h2><p>Votre photo est déjà présente !</p>');
            }
        }else{
            $error = true;
            $_SESSION['flash'] = array('alert'=> 'danger','message' => '<h2>Erreur</h2><p>Votre photo est trop lourde ! (limite de 10Mo)</p>');
        }
        */


        if(!$error) {
            $_SESSION['flash'] = array('alert'=> 'success','message'=> '
                    <h2>Votre photo a bien été envoyée</h2>
                    <p>La photo n°'. $photographie->id.' devrait bientôt apparaître!</p>');
            $photographie->creerVignette($uploaddir);

            if(file_exists("/tmp/cloudInstance.serial")){


                ob_end_clean();
                ignore_user_abort();
                ob_start();
                header("Connection: close");
                //echo "<script>document.location.href='upload'</script>";
                header("Content-Length: " . ob_get_length());
                ob_end_flush();
                flush();

                $ci = unserialize(file_get_contents("/tmp/cloudInstance.serial"));
                $ci->envoyerPhoto($uploadfile);

            }

        }
    }

    public function voter($avis){
        $error = false;
        $id_photo = PhpCommunication::getInstance()->getCurrentPhoto();
        try {
            $this->votes()->attach($id_photo, array("avis" => $avis));
            PhpCommunication::getInstance()->refreshScreen();
        }catch(QueryException $e){
            $_SESSION['flash'] = array('alert'=> 'danger','message' => '
                    <h2>Erreur</h2>
                    <p>Vous avez déjà voté pour cette photo !</p>');
            $error = true;
        }

        if(!$error){
            $_SESSION['flash'] = array('alert'=> 'success','message'=> '
                    <h2>Votre vote a bien été pris en compte</h2>
                    <p>pour la photo n°'. $id_photo.'.</p>');
        }

    }

    public function votes(){
        return $this->belongsToMany('\photoparty\modeles\Photographie','Vote','ip','photo_id')->withPivot('avis');
    }

    public function elevatePrivilege($password){
        if(hash('SHA256',',#'.$password."#*")==Configuration::getConfigValue(Configuration::ADMIN_PASSWORD)) {
            $this->privilege = 1;
            return true;
        }else return false;
    }

    public function isAdmin(){
        return $this->privilege == 1;
    }

    public function redimensionerPhotographie($srcfile,$target_filename){
        /* Image resize */
        $maxHeight = PhpCommunication::getInstance()->getScreenHeight();
        $maxWidth = PhpCommunication::getInstance()->getScreenWidth();
        list($width, $height, $type, $attr) = getimagesize($srcfile);
        $size = getimagesize($srcfile);
        $scale = 1;

        if ( $width > $maxWidth || $height > $maxHeight ) {
            $scaleW = $maxWidth/$size[0];
            $scaleH = $maxHeight/$size[1];
            if($scaleW < $scaleH){
                $scale = $scaleW;
            }else{
                $scale = $scaleH;
            }
        }

        $width = floor($size[0]*$scale);
        $height= floor($size[1]*$scale);
        /* 4 bytes alignement */
        if(($width * 3) %4 != 0)
        {
            $reste = ($width * 3)%4;
            $width = $width - (4 - $reste);
        }

        if($height%2 != 0)$height-=1;

        $src = imagecreatefromstring( file_get_contents( $srcfile ) );
        $dst = imagecreatetruecolor( $width, $height );
        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
        imagedestroy( $src );
        imagepng( $dst, $target_filename, 0); // adjust format as needed
        imagedestroy( $dst );
    }
}
