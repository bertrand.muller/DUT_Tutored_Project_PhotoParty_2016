<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 24/01/16
 * Time: 18:51
 */

namespace photoparty\modeles;


use Illuminate\Database\Eloquent\Model;

class Photographie extends Model
{
    protected $table='Photographie';
    protected $primaryKey='id';
    public $timestamps=false;

    public function stockage(){
        return $this->belongsTo('\photoparty\modeles\Stockage','id_stockage','id');
    }

    public function user(){
        return $this->belongsTo('\photoparty\modeles\Utilisateur','ip_user','ipAddress');
    }

    public function votes(){
        return $this->belongsToMany('\photoparty\modeles\Utilisateur','Vote','photo_id','ip')->withPivot('avis');
    }

    public function calculerSommeVotes($nbVotesN,$nbVotesS,$nbVotesC,$nbVotesG) {

        return $nbVotesN + 2*$nbVotesS + 3*$nbVotesC + 4*$nbVotesG;

    }

    public static function nombreDeVotesAuTotal() {

        $nbVotes = 0;

        foreach(self::all() as $ph) {
            $nbVotes += count($ph->votes);
        }

        if($nbVotes != 0) {
            return $nbVotes;
        }

    }


    public static function photosPlusVotees() {

        $photos = self::all();
        $tab = array();

        foreach($photos as $ph) {
            $indexTab = 'p_' . $ph->id;
            $tab[$indexTab] = count($ph->votes);
        }

        array_multisort($tab,SORT_DESC);

        if(count($tab) <= 5) {
            return $tab;
        } else {
            return array_chunk($tab,5,true)[0];
        }

    }


    public function retournerNbVotesSelonType($lib) {

        $nbVotesType = 0;

        foreach($this->votes as $v) {
            if($v->pivot->avis == $lib) {
                $nbVotesType++;
            }
        }

        return $nbVotesType;

    }


    public static function photosMeilleursVotes() {

        $photos = self::all();
        $tab = array();

        foreach($photos as $ph) {
            $indexTab = 'p_' . $ph->id;
            $nbVotesN = $ph->retournerNbVotesSelonType('n');
            $nbVotesS = $ph->retournerNbVotesSelonType('s');
            $nbVotesC = $ph->retournerNbVotesSelonType('c');
            $nbVotesG = $ph->retournerNbVotesSelonType('g');

            $somme = $ph->calculerSommeVotes($nbVotesN,$nbVotesS,$nbVotesC,$nbVotesG);
            $tab[$indexTab] = $somme;

        }

        array_multisort($tab,SORT_DESC);

        if(count($tab) <= 5) {
            return $tab;
        } else {
            return array_chunk($tab,5,true)[0];
        }

    }

    public function creerVignette($updir) {
        $idPh = $this->id;
        $namePh = $this->path;
        $oldname = $updir.'/'.$namePh;
        $size = getimagesize($oldname);
        $basepath = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);
        $newname = $basepath.'/web/images/vignettes/thumb_'.$idPh.'.'.explode(".",$namePh)[1];
        $newh = 240;
        $w = $size[0];
        $h = $size[1];
        $neww = intval($newh * $w / $h);
        $newimage = imagecreatetruecolor($neww,$newh);
        $this->sauverVignette($oldname,$newimage,$neww,$newh,$w,$h,$newname,0);

    }

    public function sauverVignette($oldname,$newimage,$neww,$newh,$w,$h,$newname,$mode) {

        $type = exif_imagetype($oldname);

        switch($type) {
            case 1:
                $resimage = imagecreatefromgif($oldname);
                imageCopyResampled($newimage,$resimage,0,0,0,0,$neww,$newh,$w,$h);
                imagegif($newimage,$newname);
                break;

            case 3:
                $resimage = imagecreatefrompng($oldname);
                imageCopyResampled($newimage,$resimage,0,0,0,0,$neww,$newh,$w,$h);
                imagepng($newimage,$newname,9);
                break;

            default:
                $resimage = imagecreatefromjpeg($oldname);
                imageCopyResampled($newimage,$resimage,0,0,0,0,$neww,$newh,$w,$h);
                imagejpeg($newimage,$newname,85);
                break;
        }

    }


    public static function obtenirPhotosVignettes() {

        $dirname = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME'])."/web/images/vignettes/";

        $dir = opendir($dirname);
        $tabVignettes = array();
        while($file = readdir($dir)) {
            if($file != '.' && $file != '..' && !is_dir($dirname.$file)) {
                $tabVignettes[] = $dirname.$file;
            }
        }

        closedir($dir);
        return $tabVignettes;

    }

}