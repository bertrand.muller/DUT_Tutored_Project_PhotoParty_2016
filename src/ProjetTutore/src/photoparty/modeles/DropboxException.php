<?php
/**
 * Created by PhpStorm.
 * User: froogy444
 * Date: 31/03/16
 * Time: 12:25
 */

namespace photoparty\modeles;

class DropboxException extends \Exception
{

    public function __construct($err = null, $isDebug = FALSE)
    {
        if (is_null($err)) {
            $el = error_get_last();
            $this->message = $el['message'];
            $this->file = $el['file'];
            $this->line = $el['line'];
        } else
            $this->message = $err;
        self::log_error($err);
        if ($isDebug) {
            self::display_error($err, TRUE);
        }
    }

    public static function log_error($err)
    {
        error_log($err, 0);
    }

    public static function display_error($err, $kill = FALSE)
    {
        print_r($err);
        if ($kill === FALSE) {
            die();
        }
    }
}