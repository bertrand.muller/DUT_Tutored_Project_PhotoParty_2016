<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 14/02/16
 * Time: 17:20
 */

namespace photoparty\modeles;


use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    const
        ADMIN_PSEUDO=1,
        ADMIN_PASSWORD=2,
        STOCKAGE_SELECT=3,
        TRANSITION_TIME=4,
        DIAPORAMA_STATE=5,
        ETHERNET_STATE=6,
        TRANSITION_MASK=7;

    public static $transitions = array(
        "Tournoiement et Zoom" => 0x01,
        "Rotation Horizontale" => 0x02
    );

    protected $table='Configuration';
    protected $primaryKey='id';
    public $timestamps=false;

    public static function setTransition($name){
        $mask = intval(self::getConfigValue(Configuration::TRANSITION_MASK));
        $mask|= self::$transitions[$name];
        self::setConfigValue(Configuration::TRANSITION_MASK,$mask);
    }

    public static function clearTransition($name){
        $mask = intval(self::getConfigValue(Configuration::TRANSITION_MASK));
        $mask = $mask & (~self::$transitions[$name]);
        self::setConfigValue(Configuration::TRANSITION_MASK,$mask);
    }

    public static function getTransition($name){
        $mask = intval(self::getConfigValue(Configuration::TRANSITION_MASK));
        return ($mask & Configuration::$transitions[$name]) != 0;
    }

    public static function getTransitionMask(){
        $mask = intval(self::getConfigValue(Configuration::TRANSITION_MASK));
        $result = array();
        foreach(self::$transitions as $key=>$value)
            $result[$key] = (($mask & $value)!=0);
        return $result;
    }

    public static function stockage(){
        $stockage_id = self::getConfigValue(Configuration::STOCKAGE_SELECT);
        return Stockage::find($stockage_id);
    }

    public static function changePassword($oldpassword,$newpassword){
        $password = self::getConfigValue(Configuration::ADMIN_PASSWORD);
        if(hash('sha256',',#'.$oldpassword."#*") == $password){
            self::setConfigValue(Configuration::ADMIN_PASSWORD,hash('sha256',',#'.$newpassword.'#*'));
            return true;
        }else
            return false;
    }

    public static function getConfigValue($id){
        return Configuration::find($id)->config_value;
    }

    public static function setConfigValue($id,$value){
        $conf = Configuration::find($id);
        $conf->config_value = $value;
        $conf->save();
    }
}