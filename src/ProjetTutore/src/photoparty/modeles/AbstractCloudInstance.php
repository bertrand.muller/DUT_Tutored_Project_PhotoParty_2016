<?php

namespace photoparty\modeles;

abstract class AbstractCloudInstance

{

    public abstract function getListeFichiers();

    public abstract function getFichier($id);

    public abstract function envoyerPhoto($pathTofile);

    public abstract function getUserName();

    public abstract function getTotalSpace($format = 'number');

    public abstract function getUsedSpace($format = 'number');

    public abstract function getFreeSpace($format = 'number');

    public abstract function getClassName();


    public function byteConverter($nombre){

        if($nombre == 0){

            return '0MB';

        }


        $unit = intval(log($nombre, 1024));
        $units = array('B', 'KB', 'MB', 'GB');

        $res = ($nombre / pow(1024, $unit));

        return sprintf('%s %s', number_format(floor($res*100)/100,2) , $units[$unit]);


    }




}
