<?php

namespace photoparty\modeles;


class DropBoxInstance extends AbstractCloudInstance  {


    private $dbxClient;


    public function __construct(){

        $appInfo = json_decode(file_get_contents("conf/dropBox.json"));

        $dropbox = new DropboxClient(array(
            'app_key' => $appInfo->key,
            'app_secret' => $appInfo->secret,
            'app_full_access' => false,
        ), 'en');

        $return_url = "http://photoparty.fr/adminreg";
        $authorizeUrl = $dropbox->BuildAuthorizeUrl($return_url);

        $request_token = $dropbox->GetRequestToken();
        $this->store_token($request_token, $request_token['t']);

        $this->dbxClient = $dropbox;

        echo json_encode(array('url' => $authorizeUrl));



    }


    public function createClient($token){


        $request_token = $this->load_token($token);

        $access_token = $this->dbxClient->GetAccessToken($request_token);
        $this->store_token($access_token, "access");
        $this->delete_token($token);


    }





    public function getListeFichiers()
    {

        return $this->dbxClient->GetFiles();

    }






    public function getFichier($nom)
    {

        return $this->dbxClient->DownloadFile($nom);

    }






    public function envoyerPhoto($pathTofile)
    {
               $this->dbxClient->uploadFile($pathTofile);

    }




    public function getUserName()
    {

        return $this->dbxClient->GetAccountInfo()->display_name;


    }


    public function getTotalSpace($format = 'number'){



        $espaceLibre = $this->dbxClient->GetAccountInfo()->quota_info->quota;


        if($format  != 'number'){


            $espaceLibre = $this->byteConverter($espaceLibre);

        }


        return $espaceLibre;


    }

    public function getUsedSpace($format = 'number'){

        $espaceUtilise =  $this->dbxClient->GetAccountInfo()->quota_info->normal + $this->dbxClient->getAccountInfo()->quota_info->shared;


        if($format != 'number'){


            $espaceUtilise = $this->byteConverter($espaceUtilise);

        }


        return $espaceUtilise;

    }


    public function getFreeSpace($format = 'number')
    {

        $espaceLibre = $this->getTotalSpace() - $this->getUsedSpace();


        if ($format != 'number'){


            $espaceLibre = $this->byteConverter($espaceLibre);

        }

        return $espaceLibre;

    }

    public function getClassName()
    {
        return "DropBoxInstance";
    }

    public function store_token($token, $name)
    {
        file_put_contents("/tmp/$name.token", serialize($token));
    }

    public function load_token($name)
    {
        if(!file_exists("/tmp/$name.token")) return null;
        return @unserialize(@file_get_contents("/tmp/$name.token"));
    }

    public function delete_token($name)
    {
	@unlink("/tmp/$name.token");
}
}
