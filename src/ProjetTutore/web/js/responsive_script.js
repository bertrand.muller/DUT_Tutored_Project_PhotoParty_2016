/**
 * Created by MathieuBourgault on 29/01/2016.
 */

function resize(){
    var  r = function (){
        try{document.getElementById("pb").style.height = +(window.innerHeight)-42+"px";}catch(TypeError){}
        try{document.getElementById("pi").style.height = +(window.innerHeight)-42-100+"px";}catch(TypeError){}
        try{document.getElementById("previewImage").style.height = +(window.innerHeight)-42-100-30+"px";}catch(TypeError){}
        try{document.getElementsByClassName("progress")[0].style.marginTop = (((window.innerHeight)-42)/2)-100+"px";}catch(TypeError){}
        try{document.getElementById("sidelink").style.height = +(window.innerHeight)+"px";}catch(TypeError){}
    };
    r();
    window.addEventListener("resize",r);
}
