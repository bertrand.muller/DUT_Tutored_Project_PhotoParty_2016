
$(document).ready(function() {

   $('#divCloud1').on('click',function() {

       $.ajax({
           url : "/adminreg?typeCloud=dropbox",
           type : 'POST',
           dataType : 'json',
           success : (function(data) {
              document.location.href = data.url;
           }),
           xhrFields : {
               withCredentials : true
           },
           crossDomain: true
       });

   });

   $('#divCloud2').on('click',function() {

       $.ajax({
           url : "/adminreg?typeCloud=google",
           type : 'POST',
           dataType : 'json',
           success : (function(data) {
               document.location.href = data.url;
           }),
           xhrFields : {
               withCredentials : true
           },
           crossDomain: true
       });

   });

    $('#divCloud3').on('click',function() {

        $.ajax({
            url : "/adminreg?typeCloud=onedrive",
            type : 'POST',
            dataType : 'json',
            success : (function(data) {
                document.location.href = data.url;
            }),
            xhrFields : {
                withCredentials : true
            },
            crossDomain: true
        });

    });

    $('#deconnexionCloud').on('click',function() {

        $.ajax({
            url : "/adminreg?cloudDeconnexion=1",
            type : 'POST',
            dataType : 'html',
            success : (function(data) {
                console.log('ok');
                $('#divCloudConnecte').hide();
                $('#champCloud').show();
            }),
            xhrFields : {
                withCredentials : true
            },
            crossDomain: true
        });

    });

});