$(document).ready(function() {

    var getURLParameter = function(paramName) {

        var urlVars = window.location.search.substring(1).split('&');

        for(var i = 0; i < urlVars.length; i++) {

            var tmp = urlVars[i].split('=');

            if(tmp[0] == paramName) {
                return tmp[1];
            }

        }

    };

    $('#imgEth').on('click',function() {

        var link = "/adminreg";
        var objEth = $(this);
        var pagination = getURLParameter('p');
        var pagiOk = false;

        if(pagination != null) {
            link += "?p=" + pagination;
            pagiOk = true;
        }

        if(pagiOk) {
            link += '&eth=';
        } else {
            link += '?eth=';
        }

        if(objEth.attr('src').indexOf("ethernet1") != -1) {

            link += '0';
            objEth.attr('src','web/images/ethernet2.png');

        } else {

            link += '1';
            objEth.attr('src','web/images/ethernet1.png');

        }

        $.ajax({
            url : link,
            type : 'POST',
            dataType : 'json',
            xhrFields : {
                withCredentials : true
            },
            crossDomain: true
        });

    });

});



