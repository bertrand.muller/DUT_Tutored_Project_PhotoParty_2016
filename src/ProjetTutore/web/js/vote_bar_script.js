function votePage() {

     total = 20;
    var liste = ["n", "s", "c", "g"];
    var select = checkedList(liste);
    var widthBar = barList(liste);

    totalChange(total);

    var radios = document.forms["avis"].elements["avis"];

    for (var i = 0; i < radios.length; i++) {
        radios[i].onclick = function () {
            var total;
            switch (this.value) {
                case "n":
                    total = 20;
                    break;
                case "s":
                    total = 40;
                    break;
                case "c":
                    total = 70;
                    break;
                case "g":
                    total = 100;
                    break;
            }
            totalChange(total);
        }
    }



    window.addEventListener('devicemotion', function (event) {
        var a = event.acceleration.x;

        if (a > 2) {
            total++;
            totalChange(total);
        }

    });



    function totalChange(total) {
        // radioS();
        //document.getElementById("affiche").innerHTML = ""+total;
        if (total <= 35) {
            widthBar("bn", total);
            select("rn");
        } else if (total <= 65) {
            widthBar("bs", total);
            select("rs");
        } else if (total <= 99) {
            widthBar("bc", total);
            select("rc");
        } else if (total <= 150) {
            widthBar("bg", total);
            select("rg");
        } else {
            total = 5;
            widthBar("bn", total);
            select("rn");
        }
        return total;
    }


    function barList(list) {
        return function (id, pourcentage) {

            var f = function (element) {
                if ("b" + element === id) {
                    total = pourcentage;
                    document.getElementById("b" + element).style.width = pourcentage + "%";
                } else {
                    document.getElementById("b" + element).style.width = "0%";

                }
            };
            list.forEach(f);
        }
    }

}


function radioS() {
    var s;
    var radios = document.forms["avis"].elements["avis"];
    for (var i = 0; i < radios.length; i++) {
        s += "<br>" + radios[i].value + "  " + document.getElementById(radios[i].id).innerHTML;
    }
    document.getElementById("affiche").innerHTML = s;
}


function checkedList(list) {
    return function (id) {
        var a;

        var f = function (element) {
            if ("r" + element === id) {
                a = function () {
                    document.getElementById("r" + element).setAttribute("checked", "true");
                }
            } else {
                document.getElementById("r" + element).removeAttribute("checked");
            }
        };
        list.forEach(f);
        a();
    }
}
