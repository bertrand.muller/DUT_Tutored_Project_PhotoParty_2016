var bradioSD = document.getElementById("bradio1");
var bradioDD = document.getElementById("bradio2");
var bradioCloud = document.getElementById("bradio3");
var divErreur =  document.getElementById("messageErreurStock");
var ethernet = document.getElementById("ethernet");
var divErreurDisques = document.getElementById("messageErreurDisques");
var disques = document.getElementById("disquesDispos");
var cloud = document.getElementById("champCloud");
var cloudConnecte = document.getElementById("divCloudConnecte");
var mode = 2;

if(bradioSD.checked) {
    mode = 0;
}

if(bradioDD.checked) {
    mode = 1;
}

if(bradioCloud.checked) {
    if(cloudConnecte.style.display == "none") {
        cloud.style.display = "block";
    }
}

bradioSD.onclick = function() {
    mode = 0;
    divErreur.style.display = "none";
    cloud.style.display = "none";

    if(divErreurDisques != null) {
        divErreurDisques.style.display = "none";
    }

    if(disques != null) {
        disques.style.display = "none";
    }
};

bradioDD.onclick = function() {

    var selecSD = (mode == 0);
    mode = 1;
    divErreur.style.display = "none";
    cloud.style.display = "none";

    if(divErreurDisques != null) {
        divErreurDisques.style.display = "block";
        bradioDD.checked = false;

        if(selecSD) {
            bradioSD.checked = true;
            mode = 0;
        } else {
            bradioCloud.checked = true;
            mode = 2;
        }
    }

    if(disques != null) {
        disques.style.display = "block";
    }
};


bradioCloud.onclick = function() {

    if((mode == 0) || (mode == 1) || (mode == 3)) {

        if (bradioCloud.checked) {

            if(divErreurDisques != null) {
                divErreurDisques.style.display = "none";
            }

            if(disques != null) {
                disques.style.display = "none";
            }

            if (!ethernet.checked) {

                bradioCloud.checked = false;

                if(mode == 0) {
                    bradioSD.checked = true;
                } else {
                    bradioDD.checked = true;
                }

                divErreur.style.display = "block";

            } else {
                mode = 3;
                cloud.style.display = "block";
            }
        }
    }
};

ethernet.onclick = function() {

    if(!ethernet.checked) {

        if(bradioCloud.checked) {
            bradioCloud.checked = false;
            bradioSD.checked = true;
            divErreur.style.display = "block";
        }

    } else {
        divErreur.style.display = "none";
    }

};


