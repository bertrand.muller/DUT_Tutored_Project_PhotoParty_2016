/**
 * Cette fonction permet de lire l'image de l'input
 * et de l'afficher lors de son ajout
 **/
function preview(element){
	if(element != null){

		if(document.getElementById('previewImage').src != "") {
			$('#previewImage').fadeTo(400, 0 ,function() {
				document.getElementById('previewImage').src = window.URL.createObjectURL(element);
			});
		}else{
			document.getElementById('previewImage').src = window.URL.createObjectURL(element);
		}
		document.getElementById('sendbutton').disabled = false;
		$('#previewImage').fadeTo( 400, 1 );

	}else{

		document.getElementById('sendbutton').disabled = true;
		if(document.getElementById('previewImage').src != "") {
			$('#previewImage').fadeTo(800, 0 ,function() {
				document.getElementById('previewImage').removeAttribute('src');
			});
		}

	}
}

