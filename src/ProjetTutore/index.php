<?php

require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \photoparty\modeles\Stockage as Stockage;
session_start();

$db = new DB();
$db->addConnection(parse_ini_file('conf/photoparty.db.config.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$app = new \Slim\Slim();

$app->get('/',function() use($app) {
    $c = new \photoparty\controleurs\ControleurConnexion($app->request());
    $c->afficherFormulaireConnexion();
});


$app->get('/connexion',function() use($app) {
    $c = new \photoparty\controleurs\ControleurConnexion($app->request());
    $c->afficherFormulaireConnexion();
})->name('connexion');


$app->post('/connexion',function() use($app) {
    $c = new \photoparty\controleurs\ControleurConnexion($app->request());
    $c->seConnecter();
});

$app->get('/upload',function() use($app) {
    $c = new \photoparty\controleurs\ControleurUpload($app->request());
    $c->afficherFormulaireUpload();
})->name('upload');


$app->post('/upload',function() use($app) {
    $c = new \photoparty\controleurs\ControleurUpload($app->request());
    $c->uploader();
});

$app->get('/vote',function() use($app){
    $c = new \photoparty\controleurs\ControleurVote($app->request());
    $c->formulaireVote();
});

$app->post('/vote',function() use($app){
    $c = new \photoparty\controleurs\ControleurVote($app->request());
    $c->voter();
});

$app->get('/test/:id',function($id) use($app){
    $c = new photoparty\test\TestController($app->request());
    $c->test($id);
});

$app->get('/adminreg',function() use($app) {
    $c = new \photoparty\controleurs\ControleurAdmin($app->request());
    $c->formulaireAdminReglages();
})->name('adminreg');

$app->post('/adminreg',function() use($app) {
    $c = new \photoparty\controleurs\ControleurAdmin($app->request());
    $c->validerFormulaireAdmin();
});

$app->get('/adminstat',function() use($app) {
    $c = new \photoparty\controleurs\ControleurAdmin($app->request());
    $c->formulaireAdminStatistiques();
})->name('adminstat');

$app->get('/config',function() use($app) {
    $c = new \photoparty\controleurs\ControleurREST($app->request());
    $c->afficherConfig();
});


$app->run();