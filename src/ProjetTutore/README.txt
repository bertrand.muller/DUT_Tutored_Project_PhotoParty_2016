Configuration du dossier d'upload:

chown www-data:www-data /var/www/uploads
chmod o+r /var/www/uploads
drwxr-xr-x 2 www-data www-data 4096 janv. 26 18:51 uploads

Configuration appache:
<Directory /var/www/uploads/>
	Options -Indexes
	AllowOverride None
	Order allow,deny
	Allow from all
</Directory>
<Directory />
	Options FollowSymLinks
	AllowOverride All
</Directory>

<Directory /var/www/ProjetTutore>
	Options Indexes FollowSymLinks MultiViews
	AllowOverride All
	Order allow,deny
	allow from all
</Directory>



Configuration SQLite:
Pour créer la base de donnée télécharger 
SQLiteBrowser : http://sqlitebrowser.org/
Executer le script SQL dans Script/BDD.sql
La base de donnée doit être nommée bdd.sqlite et doit être placé dans
la racine du projet (ProjetTutore/bdd.sqlite)
sous linux la base de donnée
Configurer le fichier conf/photoparty.db.config.ini
(Sous linux rajouter les droit d'écritures chmod og+w bdd.sqlite)
