# DUT - Tutored Project 2016 - _PhotoParty_  

Groupe S3D

Thomas DUBIER

Florian LEDOUX

Bertrand MULLER

Mathieu BOURGAULT

Jorge DOS SANTOS DA SILVA MOURA

Sujet : PhotoParty

Tuteur : Emmanuel Nataf

Structure du projet :

* Le dossier src contient
	* un projet eclipse pour communiquer avec le serveur php et projeter une image à l'écran .
	* un projet phpstorm, l'application en php pour recevoir les photos .
* Chaque projet possède un dossier doc contenant des schémas techniques pour implémenter les différentes classes .